#ifndef STEGANOGRAPHY_H
#define STEGANOGRAPHY_H
#define STEP 50
#include <QMainWindow>
#include <QFile>
namespace Ui {
    class Steganography;
}

class Steganography : public QMainWindow
{
    Q_OBJECT

public:
    explicit Steganography(QWidget *parent = 0);
    ~Steganography();

private slots:
    void on_hideButton_clicked();

    void on_showButton_clicked();

private:
    int tmp;
    Ui::Steganography *ui;    
    char * inputFile;
    qint64 lenOfFile;
    void writeText();
    void readText();
};

#endif // STEGANOGRAPHY_H
