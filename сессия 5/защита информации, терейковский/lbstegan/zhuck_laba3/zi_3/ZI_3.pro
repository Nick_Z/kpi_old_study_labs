#-------------------------------------------------
#
# Project created by QtCreator 2011-11-30T20:30:16
#
#-------------------------------------------------

QT       += core gui

TARGET = ZI_3
TEMPLATE = app


SOURCES += main.cpp\
        steganography.cpp

HEADERS  += steganography.h

FORMS    += steganography.ui
