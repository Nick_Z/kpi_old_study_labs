#include "steganography.h"
#include "ui_steganography.h"
#include <string>
using namespace std;
Steganography::Steganography(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Steganography)
{
    ui->setupUi(this);
    connect(ui->hideButton, SIGNAL(clicked()), this, SLOT(on_hideButton_clicked()));
    tmp = 7000;


}

Steganography::~Steganography()
{
    delete ui;    
}

void Steganography::on_hideButton_clicked()
{
    QFile picFile(ui->fileNameEdit->text());
    if (!picFile.open(QIODevice::ReadOnly))
        ui->notifyLabel->setText("File cann't be opened");
    lenOfFile = picFile.size();
    QDataStream *in = new QDataStream(&picFile);
    inputFile = new char [lenOfFile];

    in->readRawData(inputFile, lenOfFile);
    writeText();

    delete in;
    picFile.close();

    if (!picFile.open(QIODevice::WriteOnly))
        ui->notifyLabel->setText("File cann't be opened");
    in = new QDataStream(&picFile);
    int error = in->writeRawData(inputFile, lenOfFile);
    if (error == -1)
        ui->notifyLabel->setText("Error");
    else
        ui->notifyLabel->setText("Done");
    picFile.close();
    delete inputFile;
}

void Steganography::writeText()
{
    int textLen = ui->hideEdit->text().size();

    QString str = ui->hideEdit->text();
    string str2 = str.toStdString();

    if ((tmp + textLen*STEP) < lenOfFile){
        inputFile[tmp] = str.size();
        inputFile[tmp + 20] = 25;
        inputFile[tmp + 80] = 34;
        for (int i = 0; i < str.size(); i++ )
            inputFile[tmp + (i + 1)*STEP] = str2[i];
    }
}

void Steganography::on_showButton_clicked()
{
    QFile picFile(ui->fileNameEdit->text());
    if (!picFile.open(QIODevice::ReadOnly))
        ui->notifyLabel->setText("File cann't be opened");
    lenOfFile = picFile.size();
    QDataStream *in = new QDataStream(&picFile);
    inputFile = new char [lenOfFile];
    in->readRawData(inputFile, lenOfFile);

    QString str;
    int a1 = inputFile[tmp + 20];
    int a2 = inputFile[tmp + 80];
    if ((a1 == 25 ) && (a2 == 34))
        for (int i = 0; i < inputFile[tmp]; i++ )
            str += inputFile[tmp + (i + 1)*STEP];

    ui->showEdit->setText(str);
    ui->notifyLabel->setText("Done");
    delete inputFile;
    delete in;
    picFile.close();
}

