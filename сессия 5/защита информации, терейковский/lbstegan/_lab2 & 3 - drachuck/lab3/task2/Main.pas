unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, pngimage, StdCtrls, ExtDlgs;

type
  TForm1 = class(TForm)
    Label1: TLabel;
    Edit1: TEdit;
    Button1: TButton;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    OpenPictureDialog1: TOpenPictureDialog;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
  private
  public
  end;

  TByte = array[0..5] of Byte;

  TSecretStream = class(TStream)
  private
    pos: Integer;
    x,y: Integer;

    procedure GetCoords;
    function ReadByte(var data: TByte): Boolean;
    function WriteByte(const data: TByte): Boolean;
    function DataToByte(const data: TByte): Byte;
    function ByteToData(value: Byte; const data: TByte): TByte;
  protected
    function GetSize: Int64; override;
    procedure SetSize(NewSize: Longint); overload; override;
    procedure SetSize(const NewSize: Int64); overload; override;
  public
    pict: TPNGObject;

    constructor Create();
    destructor Destroy(); override;

    function Read(var Buffer; Count: Longint): Longint; override;
    function Write(const Buffer; Count: Longint): Longint; override;
    function Seek(Offset: Longint; Origin: Word): Longint; overload; override;
    function Seek(const Offset: Int64; Origin: TSeekOrigin): Int64; overload; override;

    function SignatureFound: Boolean;
    function SecretSize: Integer;
    function ReadString: String;
    procedure WriteString(str: String);
  end;

var
  Form1: TForm1;
  img: TPNGObject;

  ready: Boolean;

  ss: TSecretStream;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
var
  str: String;
  w,h: Integer;
begin
  if OpenPictureDialog1.Execute then
  begin
    str := OpenPictureDialog1.FileName;
    Edit1.Text := OpenPictureDialog1.FileName;

    try
      img.LoadFromFile(str);
      ready := True;

      w := img.Width;
      h := img.Height;

      Label3.Caption := IntToStr(w) + 'x' + IntToStr(h);
      ss.pict := img;
      Label5.Caption := FloatToStrF((ss.GetSize-10) / 8, ffNumber, 10, 2);
      Label7.Caption := IntToStr(ss.SecretSize);
    except
      ready := False;
    end;
  end;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  if Edit1.Text <> '' then
    ShowMessage(ss.ReadString);
end;

procedure TForm1.Button3Click(Sender: TObject);
var
  s: String;
begin
  if Edit1.Text = '' then Exit;
  s := InputBox('Lab3', 'Enter the secret', '');
  ss.WriteString(s);
  Label7.Caption := IntToStr(ss.SecretSize);
end;

procedure TForm1.Button4Click(Sender: TObject);
begin
  if Edit1.Text <> '' then
    img.SaveToFile(Edit1.Text);
end;

procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ss.Free;
  img.Free;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  ready := False;

  img := TPNGObject.Create;
  ss := TSecretStream.Create;
end;




{ TSecretStream }

constructor TSecretStream.Create;
begin
  pict := nil;
  pos := 0;
end;

destructor TSecretStream.Destroy;
begin
  inherited;
end;

procedure TSecretStream.GetCoords;
var
  p: Integer;
begin
  p := pos * 2;
  y := p div pict.Width;
  x := p - (y*pict.Width);
end;

function TSecretStream.GetSize: Int64;
begin
  Result := Trunc(pict.Width * pict.Height / 2);
end;

function TSecretStream.Seek(Offset: Integer; Origin: Word): Longint;
var
  np: Integer;
begin
  case Origin of
    soFromBeginning: np := Offset;
    soFromCurrent: np := pos + Offset;
    soFromEnd: np := GetSize + Offset;
  end;

  if (np >= 0) and (np < GetSize) then pos := np;
  Result := pos;
end;

function TSecretStream.Seek(const Offset: Int64; Origin: TSeekOrigin): Int64;
var
  int: Integer;
  orr: Word;
begin
  int := Offset;
  orr := Word(Origin);
  Seek(int, orr);
end;

procedure TSecretStream.SetSize(NewSize: Integer);
begin
  inherited;
end;

procedure TSecretStream.SetSize(const NewSize: Int64);
begin
  inherited;
end;

function min(a,b: Integer): Integer;
begin
  if a < b then Result := a else Result := b;
end;

function max(a,b: Integer): Integer;
begin
  if a > b then Result := a else Result := b;
end;


function TSecretStream.WriteByte(const data: TByte): Boolean;
var
  rest: Integer;
  i, count1, count2: Integer;
  index: Integer;
  color: Integer;
  r,g,b: Byte;
begin
  Result := False;
  if (GetSize - pos) < 2 then Exit;

  GetCoords;
  rest := pict.Width - x;
  count1 := min(2, rest);
  count2 := 2 - count1;

  index := 0;
  for i := 0 to count1-1 do
  begin
    r := data[index];
    g := data[index+1];
    b := data[index+2];
    color := RGB(r,g,b);
    pict.Pixels[x+i,y] := color;
    index := index + 3;
  end;

  Inc(y);
  x := 0;
  for i := 0 to count2-1 do
  begin
    r := data[index];
    g := data[index+1];
    b := data[index+2];
    color := RGB(r,g,b);
    pict.Pixels[x+i,y] := color;
    index := index + 3;
  end;

  Result := True;
end;

function TSecretStream.ReadByte(var data: TByte): Boolean;
var
  rest: Integer;
  i, count1, count2: Integer;
  index: Integer;
  color: Integer;
  temp: TByte;
begin
  Result := False;
  if (GetSize - pos) < 2 then Exit;

  GetCoords;
  rest := pict.Width - x;
  count1 := min(2, rest);
  count2 := 2 - count1;

  index := 0;
  for i := 0 to count1-1 do
  begin
    color := ColorToRGB(pict.Pixels[x+i,y]);
    temp[index] := GetRValue(color);
    temp[index+1] := GetGValue(color);
    temp[index+2] := GetBValue(color);
    index := index + 3;
  end;

  Inc(y);
  x := 0;
  for i := 0 to count2-1 do
  begin
    color := ColorToRGB(pict.Pixels[x+i,y]);
    temp[index] := GetRValue(color);
    temp[index+1] := GetGValue(color);
    temp[index+2] := GetBValue(color);
    index := index + 3;
  end;

  data := temp;

  Result := True;
end;


function TSecretStream.Write(const Buffer; Count: Integer): Longint;
var
  i: Integer;
  data, rsl: TByte;
  value: Byte;
  wr: Integer;
begin
  wr := 0;
  for i := 0 to Count-1 do
  begin
    if ReadByte(data) then
    begin
      value := Byte((Pointer(Cardinal(Pointer(@Buffer)) + wr))^);
      rsl := ByteToData(value, data);
      WriteByte(rsl);
      Inc(wr);
      Inc(pos);
    end;
  end;

  Result := wr;
end;

function TSecretStream.Read(var Buffer; Count: Integer): Longint;
var
  i: Integer;

  data: TByte;
  value: Byte;
  rd: Integer;
begin
  rd := 0;
  for i := 0 to Count-1 do
  begin
    if ReadByte(data) then
    begin
      value := DataToByte(data);
      Byte((Pointer(Cardinal(Pointer(@Buffer)) + rd))^) := value;
      Inc(rd);
      Inc(pos);
    end;
  end;

  Result := rd;
end;



function TSecretStream.DataToByte(const data: TByte): Byte;
begin
 asm
  xor dl, dl
  mov ebx, data
  mov al, byte ptr [ebx]
  and al, 3
  or dl, al
  ror dl, 2

  inc ebx
  mov al, byte ptr [ebx]
  and al, 1
  or dl, al
  ror dl, 1

  inc ebx
  mov al, byte ptr [ebx]
  and al, 1
  or dl, al
  ror dl, 1



  inc ebx
  mov al, byte ptr [ebx]
  and al, 3
  or dl, al
  ror dl, 2

  inc ebx
  mov al, byte ptr [ebx]
  and al, 1
  or dl, al
  ror dl, 1

  inc ebx
  mov al, byte ptr [ebx]
  and al, 1
  or dl, al
  ror dl, 1

  mov Result, dl
 end;
end;

function TSecretStream.ByteToData(value: Byte; const data: TByte): TByte;
begin
  Result := data;

  asm
    mov dl, value
    mov ebx, Result

    and byte ptr [ebx], 11111100b
    mov dh, dl
    and dh, 3
    or byte ptr [ebx], dh
    shr dl, 2

    inc ebx
    and byte ptr [ebx], 11111110b
    mov dh, dl
    and dh, 1
    or byte ptr [ebx], dh
    shr dl, 1

    inc ebx
    and byte ptr [ebx], 11111110b
    mov dh, dl
    and dh, 1
    or byte ptr [ebx], dh
    shr dl, 1


    inc ebx
    and byte ptr [ebx], 11111100b
    mov dh, dl
    and dh, 3
    or byte ptr [ebx], dh
    shr dl, 2

    inc ebx
    and byte ptr [ebx], 11111110b
    mov dh, dl
    and dh, 1
    or byte ptr [ebx], dh
    shr dl, 1

    inc ebx
    and byte ptr [ebx], 11111110b
    mov dh, dl
    and dh, 1
    or byte ptr [ebx], dh
    shr dl, 1
  end;
end;


function TSecretStream.SignatureFound: Boolean;
var
  sig: array[1..7] of char;
  pos: Integer;
begin
  sig[7] := #0;
  pos := Position;
  Seek(0, 0);
  Read(sig, 6);
  Result := CompareMem(@sig, PChar('Secret'), 6);
  Seek(pos, 0);
end;

function TSecretStream.SecretSize: Integer;
var
  pos: Integer;
  size: Integer;
begin
  if SignatureFound then
  begin
    pos := Position;
    Seek(6, soFromBeginning);
    if Read(size, 4) = 4 then Result := size else Result := 0;
    Seek(pos, soFromBeginning);
  end
 else Result := 0;
end;

function TSecretStream.ReadString: String;
var
  pos: Integer;
  ps: PChar;
  ssize: Integer;
begin
  Result := '';
  ssize := SecretSize;
  if ssize = 0 then Exit;
  ps := AllocMem(ssize+1);

  pos := Position;
  Seek(10, soFromBeginning);
  Read(ps^, ssize);
  Seek(pos, soFromBeginning);

  Byte((Pointer(Cardinal(Pointer(ps)) + ssize))^) := 0;
  Result := String(ps);
end;

procedure TSecretStream.WriteString(str: String);
var
  t: String;
  sig: array [1..6] of char;
  size: Integer;
  ps: Pointer;
begin
  t := 'Secret';
  CopyMemory(@sig, PChar(t), 6);
  size := Length(str);

  Seek(0, soFromBeginning);
  Write(sig, 6);
  Write(size, 4);
  ps := PChar(str);
  Write(ps^, size);
end;



end.
