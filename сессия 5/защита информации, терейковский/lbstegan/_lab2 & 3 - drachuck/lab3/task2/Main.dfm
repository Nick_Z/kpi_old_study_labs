object Form1: TForm1
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'Lab3'
  ClientHeight = 105
  ClientWidth = 655
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 51
    Height = 13
    Caption = 'Image file:'
  end
  object Label2: TLabel
    Left = 8
    Top = 40
    Width = 55
    Height = 13
    Caption = 'Image size:'
  end
  object Label3: TLabel
    Left = 69
    Top = 40
    Width = 18
    Height = 13
    Caption = 'N\A'
  end
  object Label4: TLabel
    Left = 8
    Top = 59
    Width = 133
    Height = 13
    Caption = 'Available secret space (kb):'
  end
  object Label5: TLabel
    Left = 144
    Top = 59
    Width = 18
    Height = 13
    Caption = 'N\A'
  end
  object Label6: TLabel
    Left = 8
    Top = 78
    Width = 130
    Height = 13
    Caption = 'Used secret space (bytes):'
  end
  object Label7: TLabel
    Left = 144
    Top = 78
    Width = 18
    Height = 13
    Caption = 'N\A'
  end
  object Edit1: TEdit
    Left = 65
    Top = 5
    Width = 504
    Height = 21
    ReadOnly = True
    TabOrder = 0
  end
  object Button1: TButton
    Left = 575
    Top = 3
    Width = 75
    Height = 25
    Caption = 'Open'
    TabOrder = 1
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 504
    Top = 73
    Width = 65
    Height = 25
    Caption = 'Read'
    TabOrder = 2
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 575
    Top = 73
    Width = 75
    Height = 25
    Caption = 'Write'
    TabOrder = 3
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 575
    Top = 28
    Width = 75
    Height = 25
    Caption = 'Save'
    TabOrder = 4
    OnClick = Button4Click
  end
  object OpenPictureDialog1: TOpenPictureDialog
    Filter = 'PNG Images|*.png|All files (*.*)|*.*'
    Options = [ofReadOnly, ofHideReadOnly, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Left = 592
    Top = 48
  end
end
