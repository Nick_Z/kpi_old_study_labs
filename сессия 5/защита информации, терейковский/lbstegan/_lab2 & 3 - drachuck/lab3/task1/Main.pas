unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TForm1 = class(TForm)
    Label1: TLabel;
    Edit1: TEdit;
    Label2: TLabel;
    Edit2: TEdit;
    Button1: TButton;
    Button2: TButton;
    Label3: TLabel;
    Edit3: TEdit;
    Button3: TButton;
    Button4: TButton;
    OpenDialog1: TOpenDialog;
    OpenDialog2: TOpenDialog;
    SaveDialog1: TSaveDialog;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
  private
  public
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
begin
  if OpenDialog1.Execute() then
    Edit1.Text := OpenDialog1.FileName;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
if OpenDialog2.Execute() then
    Edit2.Text := OpenDialog2.FileName;
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
  if SaveDialog1.Execute() then
    Edit3.Text := SaveDialog1.FileName;
end;

procedure TForm1.Button4Click(Sender: TObject);
const
  buf_size = 1024;
var
  f1, f2: file of byte;
  buffer: array [0..buf_size-1] of byte;
  rd: Integer;
begin
  if (not FileExists(Edit1.Text)) or (not FileExists(Edit2.Text)) then
  begin
    MessageDlg('Input files wasn''t found', mtWarning, [mbOK], 0);
    Exit;
  end;

  if FileExists(Edit3.Text) then
    if MessageDlg('Output files already exists. Overwrite?', mtConfirmation, [mbYes, mbCancel], 0) <> mrYes then Exit;


  {$I-}
  AssignFile(f1, Edit1.Text);
  Reset(f1);
  if IOResult <> 0 then
  begin
    MessageDlg('First file wan''t opened!', mtError, [mbOK], 0);
    CloseFile(f1);
    Exit;
  end;

  AssignFile(f2, Edit3.Text);
  Rewrite(f2);
  if IOResult <> 0 then
  begin
    MessageDlg('Output file wasn''t created!', mtError, [mbOK], 0);
    CloseFile(f1);
    CloseFile(f2);
    Exit;
  end;

  repeat
    BlockRead(f1, buffer, buf_size, rd);
    BlockWrite(f2, buffer, rd);
  until rd = 0;

  CloseFile(f1);
  AssignFile(f1, Edit2.Text);
  Reset(f1);
  if IOResult <> 0 then
  begin
    MessageDlg('Seconds file wan''t opened!', mtError, [mbOK], 0);
    CloseFile(f1);
    CloseFile(f2);
    Exit;
  end;

  repeat
    BlockRead(f1, buffer, buf_size, rd);
    BlockWrite(f2, buffer, rd);
  until rd = 0;

  CloseFile(f1);
  CloseFile(f2);
  {$I+}

  MessageDlg('Successfully', mtInformation, [mbOK], 0);
end;

end.
