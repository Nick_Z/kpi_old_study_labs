object Form1: TForm1
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'Lab3'
  ClientHeight = 119
  ClientWidth = 612
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 46
    Height = 13
    Caption = 'JPEG-file:'
  end
  object Label2: TLabel
    Left = 8
    Top = 35
    Width = 46
    Height = 13
    Caption = 'Archieve:'
  end
  object Label3: TLabel
    Left = 8
    Top = 64
    Width = 38
    Height = 13
    Caption = 'Output:'
  end
  object Edit1: TEdit
    Left = 67
    Top = 5
    Width = 458
    Height = 21
    TabOrder = 0
  end
  object Edit2: TEdit
    Left = 67
    Top = 32
    Width = 458
    Height = 21
    TabOrder = 1
  end
  object Button1: TButton
    Left = 531
    Top = 3
    Width = 75
    Height = 25
    Caption = 'Select'
    TabOrder = 2
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 531
    Top = 30
    Width = 75
    Height = 25
    Caption = 'Select'
    TabOrder = 3
    OnClick = Button2Click
  end
  object Edit3: TEdit
    Left = 67
    Top = 61
    Width = 458
    Height = 21
    TabOrder = 4
  end
  object Button3: TButton
    Left = 531
    Top = 59
    Width = 75
    Height = 25
    Caption = 'Select'
    TabOrder = 5
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 8
    Top = 88
    Width = 75
    Height = 25
    Caption = 'Process'
    TabOrder = 6
    OnClick = Button4Click
  end
  object OpenDialog1: TOpenDialog
    Filter = 'JPEG-image (*.jpg)|*.jpg|All files (*.*)|*.*'
    Options = [ofReadOnly, ofHideReadOnly, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Left = 152
    Top = 8
  end
  object OpenDialog2: TOpenDialog
    Filter = 'ZIP-archieve (*.zip)|*.zip|All files (*.*)|*.*'
    Options = [ofReadOnly, ofHideReadOnly, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Left = 200
    Top = 16
  end
  object SaveDialog1: TSaveDialog
    Filter = 'JPEG-files (*.jpg)|*.jpg|All files (*.*)|*.*'
    Left = 248
    Top = 48
  end
end
