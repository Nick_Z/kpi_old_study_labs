#include <stdlib.h>
#include <vcclr.h>
#include "stdafx.h"
#include "string.h"
#using <mscorlib.dll>
#pragma once

namespace RGR_0 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::IO;
	using namespace System::Runtime::InteropServices;

	/// <summary>
	/// ������ ��� Form1
	///
	/// ��������! ��� ��������� ����� ����� ������ ���������� ����� ��������
	///          �������� ����� ����� �������� ("Resource File Name") ��� �������� ���������� ������������ �������,
	///          ���������� �� ����� ������� � ����������� .resx, �� ������� ������� ������ �����. � ��������� ������,
	///          ������������ �� ������ ��������� �������� � ���������������
	///          ���������, ��������������� ������ �����.
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
		public:	 System::Drawing::Bitmap^ containerFile;//���������� ��� �������� BMP ����������� (�����-����������)
				 System::Drawing::Bitmap^ cloneContainerFile;//����-��������� � ������������� �������� ����� � 24 bit (�������� ��� ����� ������)
				 System::String^ cfpath;//���� �� �������� ��������� ����-���������
				 System::Drawing::Size^ cfSize;
				 int cfSizeB, hfSizeB, cfSizeEnable, cloneHeight, cloneWidth, key;
				 //int width, height;//������� �����-����������
				 System::String^ hfpath;
				 System::String^ textToHide;//�����, ������� ����� ����������
				 System::String^ stringKey;//���������� ��� ������ �����
				 System::IO::MemoryStream^ userInput;
		private: System::Windows::Forms::RichTextBox^  richTextBoxTextToHide;
		public: 

		private: System::Windows::Forms::Label^  labelInfo;
		private: System::Windows::Forms::Label^  labelTextToHide;
		private: System::Windows::Forms::Button^  buttonSave;
	private: System::Windows::Forms::TextBox^  textBoxKey;

	public: 
		private: System::Windows::Forms::Button^  buttonExit;
		public: 
		public:
			Form1(void)
			{
				InitializeComponent();
				//
				//TODO: �������� ��� ������������
				//
			}

		protected:
			/// <summary>
			/// ���������� ��� ������������ �������.
			/// </summary>
			~Form1()
			{
				if (components)
				{
					delete components;
				}
			}
		private: System::Windows::Forms::PictureBox^  pictureBoxContainerFile;
		private: System::Windows::Forms::Label^  labelContainerFile;
		private: System::Windows::Forms::TextBox^  textBoxContainerFile;
		private: System::Windows::Forms::Button^  buttonContainerFile;
		private: System::Windows::Forms::Label^  labelKey;
		private: System::Windows::Forms::Button^  buttonDecode;
		private: System::Windows::Forms::Button^  buttonCode;
		private: System::Windows::Forms::RichTextBox^  richTextBoxInfo;
		protected: 
		private:
			/// <summary>
			/// ��������� ���������� ������������.
			/// </summary>
			System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ������������ ����� ��� ��������� ������������ - �� ���������
		/// ���������� ������� ������ ��� ������ ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
            System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(Form1::typeid));
            this->pictureBoxContainerFile = (gcnew System::Windows::Forms::PictureBox());
            this->labelContainerFile = (gcnew System::Windows::Forms::Label());
            this->textBoxContainerFile = (gcnew System::Windows::Forms::TextBox());
            this->buttonContainerFile = (gcnew System::Windows::Forms::Button());
            this->labelKey = (gcnew System::Windows::Forms::Label());
            this->buttonDecode = (gcnew System::Windows::Forms::Button());
            this->buttonCode = (gcnew System::Windows::Forms::Button());
            this->richTextBoxInfo = (gcnew System::Windows::Forms::RichTextBox());
            this->buttonExit = (gcnew System::Windows::Forms::Button());
            this->richTextBoxTextToHide = (gcnew System::Windows::Forms::RichTextBox());
            this->labelInfo = (gcnew System::Windows::Forms::Label());
            this->labelTextToHide = (gcnew System::Windows::Forms::Label());
            this->buttonSave = (gcnew System::Windows::Forms::Button());
            this->textBoxKey = (gcnew System::Windows::Forms::TextBox());
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBoxContainerFile))->BeginInit();
            this->SuspendLayout();
            // 
            // pictureBoxContainerFile
            // 
            this->pictureBoxContainerFile->BackgroundImage = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"pictureBoxContainerFile.BackgroundImage")));
            this->pictureBoxContainerFile->BackgroundImageLayout = System::Windows::Forms::ImageLayout::Center;
            this->pictureBoxContainerFile->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
            this->pictureBoxContainerFile->Location = System::Drawing::Point(12, 12);
            this->pictureBoxContainerFile->Name = L"pictureBoxContainerFile";
            this->pictureBoxContainerFile->Size = System::Drawing::Size(300, 180);
            this->pictureBoxContainerFile->SizeMode = System::Windows::Forms::PictureBoxSizeMode::Zoom;
            this->pictureBoxContainerFile->TabIndex = 0;
            this->pictureBoxContainerFile->TabStop = false;
            // 
            // labelContainerFile
            // 
            this->labelContainerFile->AutoSize = true;
            this->labelContainerFile->Location = System::Drawing::Point(12, 208);
            this->labelContainerFile->Name = L"labelContainerFile";
            this->labelContainerFile->Size = System::Drawing::Size(145, 13);
            this->labelContainerFile->TabIndex = 1;
            this->labelContainerFile->Text = L"�������� ����-���������:";
            // 
            // textBoxContainerFile
            // 
            this->textBoxContainerFile->BackColor = System::Drawing::SystemColors::Window;
            this->textBoxContainerFile->Location = System::Drawing::Point(12, 224);
            this->textBoxContainerFile->Name = L"textBoxContainerFile";
            this->textBoxContainerFile->ReadOnly = true;
            this->textBoxContainerFile->Size = System::Drawing::Size(247, 20);
            this->textBoxContainerFile->TabIndex = 2;
            // 
            // buttonContainerFile
            // 
            this->buttonContainerFile->Location = System::Drawing::Point(265, 224);
            this->buttonContainerFile->Name = L"buttonContainerFile";
            this->buttonContainerFile->Size = System::Drawing::Size(47, 20);
            this->buttonContainerFile->TabIndex = 1;
            this->buttonContainerFile->Text = L"�����";
            this->buttonContainerFile->UseVisualStyleBackColor = true;
            this->buttonContainerFile->Click += gcnew System::EventHandler(this, &Form1::buttonContainerFile_Click);
            // 
            // labelKey
            // 
            this->labelKey->AutoSize = true;
            this->labelKey->Location = System::Drawing::Point(12, 272);
            this->labelKey->Name = L"labelKey";
            this->labelKey->Size = System::Drawing::Size(242, 26);
            this->labelKey->TabIndex = 7;
            this->labelKey->Text = L"������� ����, ���� ��������� ������������ \n����-���������:";
            // 
            // buttonDecode
            // 
            this->buttonDecode->AutoSize = true;
            this->buttonDecode->Enabled = false;
            this->buttonDecode->Location = System::Drawing::Point(108, 395);
            this->buttonDecode->Name = L"buttonDecode";
            this->buttonDecode->Size = System::Drawing::Size(93, 25);
            this->buttonDecode->TabIndex = 6;
            this->buttonDecode->Text = L"������������";
            this->buttonDecode->UseVisualStyleBackColor = true;
            this->buttonDecode->Click += gcnew System::EventHandler(this, &Form1::buttonDecode_Click);
            // 
            // buttonCode
            // 
            this->buttonCode->AutoSize = true;
            this->buttonCode->Enabled = false;
            this->buttonCode->Location = System::Drawing::Point(12, 395);
            this->buttonCode->Name = L"buttonCode";
            this->buttonCode->Size = System::Drawing::Size(90, 25);
            this->buttonCode->TabIndex = 3;
            this->buttonCode->Text = L"�����������";
            this->buttonCode->UseVisualStyleBackColor = true;
            this->buttonCode->Click += gcnew System::EventHandler(this, &Form1::buttonCode_Click);
            // 
            // richTextBoxInfo
            // 
            this->richTextBoxInfo->BackColor = System::Drawing::SystemColors::Info;
            this->richTextBoxInfo->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
            this->richTextBoxInfo->ForeColor = System::Drawing::SystemColors::InfoText;
            this->richTextBoxInfo->Location = System::Drawing::Point(348, 28);
            this->richTextBoxInfo->Name = L"richTextBoxInfo";
            this->richTextBoxInfo->ReadOnly = true;
            this->richTextBoxInfo->ScrollBars = System::Windows::Forms::RichTextBoxScrollBars::Vertical;
            this->richTextBoxInfo->Size = System::Drawing::Size(300, 150);
            this->richTextBoxInfo->TabIndex = 12;
            this->richTextBoxInfo->Text = L"> �������� ������ ����������� (����-���������)\n\n";
            // 
            // buttonExit
            // 
            this->buttonExit->Location = System::Drawing::Point(558, 395);
            this->buttonExit->Name = L"buttonExit";
            this->buttonExit->Size = System::Drawing::Size(90, 25);
            this->buttonExit->TabIndex = 7;
            this->buttonExit->Text = L"�����";
            this->buttonExit->UseVisualStyleBackColor = true;
            this->buttonExit->Click += gcnew System::EventHandler(this, &Form1::buttonExit_Click);
            // 
            // richTextBoxTextToHide
            // 
            this->richTextBoxTextToHide->BackColor = System::Drawing::SystemColors::Window;
            this->richTextBoxTextToHide->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
            this->richTextBoxTextToHide->ForeColor = System::Drawing::SystemColors::WindowText;
            this->richTextBoxTextToHide->Location = System::Drawing::Point(348, 226);
            this->richTextBoxTextToHide->Name = L"richTextBoxTextToHide";
            this->richTextBoxTextToHide->ScrollBars = System::Windows::Forms::RichTextBoxScrollBars::Vertical;
            this->richTextBoxTextToHide->Size = System::Drawing::Size(300, 150);
            this->richTextBoxTextToHide->TabIndex = 2;
            this->richTextBoxTextToHide->Text = L"";
            this->richTextBoxTextToHide->TextChanged += gcnew System::EventHandler(this, &Form1::richTextBoxTextToHide_TextChanged);
            // 
            // labelInfo
            // 
            this->labelInfo->AutoSize = true;
            this->labelInfo->Location = System::Drawing::Point(345, 12);
            this->labelInfo->Name = L"labelInfo";
            this->labelInfo->Size = System::Drawing::Size(130, 13);
            this->labelInfo->TabIndex = 15;
            this->labelInfo->Text = L"��������� ���������:";
            // 
            // labelTextToHide
            // 
            this->labelTextToHide->AutoSize = true;
            this->labelTextToHide->Location = System::Drawing::Point(345, 208);
            this->labelTextToHide->Name = L"labelTextToHide";
            this->labelTextToHide->Size = System::Drawing::Size(229, 13);
            this->labelTextToHide->TabIndex = 16;
            this->labelTextToHide->Text = L"����� ����������� (��������) ���������:";
            // 
            // buttonSave
            // 
            this->buttonSave->AutoSize = true;
            this->buttonSave->Enabled = false;
            this->buttonSave->Location = System::Drawing::Point(222, 395);
            this->buttonSave->Name = L"buttonSave";
            this->buttonSave->Size = System::Drawing::Size(90, 25);
            this->buttonSave->TabIndex = 4;
            this->buttonSave->Text = L"���������";
            this->buttonSave->UseVisualStyleBackColor = true;
            this->buttonSave->Click += gcnew System::EventHandler(this, &Form1::buttonSave_Click);
            // 
            // textBoxKey
            // 
            this->textBoxKey->Location = System::Drawing::Point(12, 301);
            this->textBoxKey->Name = L"textBoxKey";
            this->textBoxKey->Size = System::Drawing::Size(247, 20);
            this->textBoxKey->TabIndex = 5;
            this->textBoxKey->TextChanged += gcnew System::EventHandler(this, &Form1::textBoxKey_TextChanged);
            // 
            // Form1
            // 
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->AutoScroll = true;
            this->ClientSize = System::Drawing::Size(664, 459);
            this->Controls->Add(this->buttonSave);
            this->Controls->Add(this->labelTextToHide);
            this->Controls->Add(this->labelInfo);
            this->Controls->Add(this->richTextBoxTextToHide);
            this->Controls->Add(this->buttonExit);
            this->Controls->Add(this->richTextBoxInfo);
            this->Controls->Add(this->buttonCode);
            this->Controls->Add(this->buttonDecode);
            this->Controls->Add(this->textBoxKey);
            this->Controls->Add(this->labelKey);
            this->Controls->Add(this->buttonContainerFile);
            this->Controls->Add(this->textBoxContainerFile);
            this->Controls->Add(this->labelContainerFile);
            this->Controls->Add(this->pictureBoxContainerFile);
            this->Name = L"Form1";
            this->Text = L"DigiSteg";
            (cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBoxContainerFile))->EndInit();
            this->ResumeLayout(false);
            this->PerformLayout();

        }
	#pragma endregion
		private: int *ByteToBinary (BYTE val)
		/* ��������� �������� ���� �� ����. ���������� ������ �� ������ ���������
		   ������ N� ������� ������������� N�� ����.
		   ��������� �������� 1 ��� 0
		*/
		{           
			int *mass = new int[8];
			int t, i;
			for (t = 128, i = 0; t > 0; t /= 2, i++)
			{
				if ((val & t) != 0) 
					mass[i] = 1;
				else 
					if ((val & t) == 0) 
						mass[i] = 0;
			}
		  return mass;
		};

		private: BYTE BinaryToByte (int *mass)
		/* �������� �� ������� ����� ���� ����� ���� � ���������� ���.
		   ������� ��������: ������ �� ������ ���������, ���������������
		   ����� �����
		*/
		{
			BYTE Mask   = 00000001;
			BYTE Result = 00000000;
			BYTE Mask2;
			int j = 0;
			for (int i = 7; i > -1; i--, j++)
			{
				if (mass[i] == 1)  
				{
					Mask2 = (Mask << (j));
					Result = Result|Mask2;
				}
			}
			return Result;
		}

		private: int GetBitValue(BYTE B, int N)
		/* �������� �������� N-��� ���� � ����� B
		   ���������� 1 ��� 0 (� ����������� �� �������� ����)
		*/
		{ 
			int k = 256;
			for (int i = 0; i < N; i++) 
				k /= 2;
			if ((B & k) != 0) 
				return 1;
			else 
				return 0;
		}

		private: BYTE ReadBitToByte (int Bit, BYTE B)
		/* ���������� � ���� B �� ��������� ������� ��� Bit
		*/
		{
			 BYTE A = 00000001;
			 BYTE Result = B;
			 if (Bit == GetBitValue(B, 8)) 
				 return B;
			 else 
				 if (Bit == 1) 
					 return Result = Result | A;
				 else 
					 if (Bit == 0) 
						 return B - A;
			 return NULL;
		}

		private: System::Void buttonContainerFile_Click(System::Object^  sender, System::EventArgs^  e) 
		{
			OpenFileDialog^ openContainerFile = gcnew OpenFileDialog;
			openContainerFile->InitialDirectory = "c:\\";
			openContainerFile->Filter = "bmp files (*.bmp)|*.bmp";
			openContainerFile->FilterIndex = 2;
			openContainerFile->RestoreDirectory = true;
			if(openContainerFile->ShowDialog() == System::Windows::Forms::DialogResult::OK)
			{
				System::IO::StreamReader^ sr = gcnew System::IO::StreamReader(openContainerFile->FileName);		
				textBoxContainerFile->Text = openContainerFile->FileName;		 
				sr->Close();
			}
			cfpath = textBoxContainerFile->Text;
			if (cfpath != "")
			{		
				delete pictureBoxContainerFile->BackgroundImage;
				pictureBoxContainerFile->BackgroundImage = nullptr;
				containerFile = gcnew System::Drawing::Bitmap(cfpath);
				RectangleF cloneCF = RectangleF(0,0,containerFile->Width,containerFile->Height);
				cloneContainerFile = containerFile->Clone(cloneCF, Imaging::PixelFormat::Format24bppRgb);
				cloneHeight = cloneContainerFile->Height;
				cloneWidth = cloneContainerFile->Width;
				pictureBoxContainerFile->SizeMode = System::Windows::Forms::PictureBoxSizeMode::Zoom;
				pictureBoxContainerFile->Image = containerFile;
				richTextBoxInfo->Text += L"> ����-��������� ������� ��������\n\n";
				cfSize = cloneContainerFile->Size;
				cfSizeB = cfSize->Height * cfSize->Width;
				cfSizeEnable = cfSizeB / 8;
				richTextBoxInfo->Text += L"> ������������ ���������� ����������, ������� ����� ������ " + Convert::ToString(cfSizeEnable) + " ����� ��� " + Convert::ToString(cfSizeEnable / 8) + " �������" + "\n\n";
				richTextBoxInfo->Text += L"> ���� ����� ����������� ���������, ������� ��� � ����, ������������� ����\n\n";
				richTextBoxInfo->Text += L"> ���� ����� ������������ ���������, ������� ���� (���� ������ ���� ������� 8)\n\n";
				if ((cfSizeEnable > hfSizeB) && (hfSizeB != 0))
					buttonCode->Enabled = true;
				richTextBoxInfo->SelectionStart = richTextBoxInfo->Text->Length;
				richTextBoxInfo->ScrollToCaret();
			}
			else
				buttonCode->Enabled = false;
			richTextBoxInfo->SelectionStart = richTextBoxInfo->Text->Length;
			richTextBoxInfo->ScrollToCaret();

		}

		private: void richTextBoxTextToHide_TextChanged(Object^ /*sender*/, EventArgs^ /*e*/ )
		{
			hfSizeB = richTextBoxTextToHide->Text->Length;
			if(hfSizeB > cfSizeEnable)
			{
				richTextBoxInfo->Text += L"> ���������� ���������� ���������� ��������� ���������� ���������� ���������� ��� ����������\n\n";
				buttonCode->Enabled = false;
				richTextBoxInfo->SelectionStart = richTextBoxInfo->Text->Length;
				richTextBoxInfo->ScrollToCaret();
			}
			if ((cfSizeEnable > hfSizeB) && (hfSizeB != 0))
				buttonCode->Enabled = true;
			if((hfSizeB == 0) && (stringKey != ""))
				buttonCode->Enabled = false;
			textToHide = richTextBoxTextToHide->Text;
		};

		private: System::Void buttonCode_Click(System::Object^  sender, System::EventArgs^  e) 
		{
			int i, j;
			int r,g,b;
			int coordX = (cloneWidth - 1) - (hfSizeB * 8) % (cloneWidth - 1);//���������� ���������� �������
			int	coordY = (cloneHeight - 1) - (hfSizeB * 8) / (cloneWidth - 1); 
			BYTE textByte;
			char* Message = (char*)(void*)Marshal::StringToHGlobalAnsi(textToHide);
			//������� ���������
			for(i = 0; i < hfSizeB; i++)// ������� ��������
			{
				textByte = Message[i];// �������� ��������� ������
				for(j = 0; j < 8; j++)// ������� ����� �������
				{
					r=cloneContainerFile->GetPixel(coordX, coordY).R;   //
					g=cloneContainerFile->GetPixel(coordX, coordY).G;   // �������� ������
					b=cloneContainerFile->GetPixel(coordX, coordY).B;	//
					int bit = GetBitValue(textByte, j+1); // �������� ������������ ���
					r = ReadBitToByte (bit, r);         // �������� j-� ��� � �����
					cloneContainerFile->SetPixel(coordX, coordY, Color::FromArgb(r,g,b));  // �������������� ����*/
					if((coordX < (cloneWidth - 1)) && (coordY <= (cloneHeight - 1)))
						++coordX;
					if ((coordX >= (cloneWidth - 1)) && (coordY < (cloneHeight - 1)))
					{
						++coordY;
						coordX = 0;
					}
				}
			}
			buttonSave->Enabled = true;
			richTextBoxInfo->Text += L"> ����� ������� ����������" + "\n\n";
			richTextBoxInfo->Text += L"> ����: " +hfSizeB*8+ "\n\n";
			richTextBoxInfo->Text += L"> ��������� �����������" + "\n\n";
			richTextBoxInfo->SelectionStart = richTextBoxInfo->Text->Length;
			richTextBoxInfo->ScrollToCaret();
		}

		private: System::Void buttonSave_Click(System::Object^  sender, System::EventArgs^  e) 
		{
			SaveFileDialog^ saveContainerFile = gcnew SaveFileDialog;
			saveContainerFile->InitialDirectory = "c:\\";
			saveContainerFile->Filter = "bmp files (*.bmp)|*.bmp";
			saveContainerFile->FilterIndex = 2;
			saveContainerFile->RestoreDirectory = true;
			saveContainerFile->AddExtension = true;
			saveContainerFile->OverwritePrompt = true;
			if(saveContainerFile->ShowDialog() == System::Windows::Forms::DialogResult::OK)
			{
				if(saveContainerFile->FileName != "")
				{
					cloneContainerFile->Save(saveContainerFile->FileName);
					richTextBoxInfo->Text += L"> ���������� ������� ���������\n\n";
					richTextBoxInfo->SelectionStart = richTextBoxInfo->Text->Length;
					richTextBoxInfo->ScrollToCaret();
				}
			}
			else
			{
				MessageBox::Show("����������� �� ���������...", "������!", MessageBoxButtons::OK, MessageBoxIcon::Exclamation);
				return;
			}
		}

		private: void textBoxKey_TextChanged(Object^ /*sender*/, EventArgs^ /*e*/ )
		{			
			int sizeKey = textBoxKey->TextLength;
			stringKey = textBoxKey->Text;
			if((stringKey != "") && (cfpath != "") && ((System::Int32::Parse(stringKey) % 8) == 0))				
				buttonDecode->Enabled = true;
			else
				buttonDecode->Enabled = false;
		}

		private: System::Void buttonDecode_Click(System::Object^  sender, System::EventArgs^  e) 
		{
			BYTE r,g,b;
			char *result = new char;			
			int Key = System::Int32::Parse(textBoxKey->Text);//�������������� �� ������ � �����			
			int coordX = (cloneWidth - 1) - Key % (cloneWidth - 1);//���������� ���������� �������
			int	coordY = (cloneHeight - 1) - Key / (cloneWidth - 1); 
			int *BitMass = new int[8]; // ������ ��� �������� �����
			for (int i = 0; i < (Key / 8); i++)  // ������� �������� ������������ ���������
			{
				for (int j = 0; j < 8; j++) // ������� ����� �������
				{
					r=cloneContainerFile->GetPixel(coordX, coordY).R;   //
					//g=cloneContainerFile->GetPixel(coordX, coordY).G;   // �������� ������
					//b=cloneContainerFile->GetPixel(coordX, coordY).B;	//
					BitMass[j] = GetBitValue(r, 8);
					if((coordX < (cloneWidth - 1)) && (coordY <= (cloneHeight - 1)))
						++coordX;
					if ((coordX >= (cloneWidth - 1)) && (coordY < (cloneHeight - 1)))
					{
						++coordY;
						coordX = 0;
					}
				}
				result[i] =  BinaryToByte(BitMass);
				for (int k = 0; k < 8; k++) BitMass[k] = 0;
			}
			richTextBoxTextToHide->Clear();
			result[Key / 8] = '\0';			
			String ^resultMessage = gcnew String(result);
			richTextBoxTextToHide->Text += resultMessage;
			richTextBoxInfo->Text += L"> ���� �� ������ �������������� ���������\n\n";
			richTextBoxInfo->SelectionStart = richTextBoxInfo->Text->Length;
			richTextBoxInfo->ScrollToCaret();
		}

		private: System::Void buttonExit_Click(System::Object^  sender, System::EventArgs^  e) 
		{
			Application::Exit();
		}	
	};
}

