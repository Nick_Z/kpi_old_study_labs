Data SEGMENT
s1 db 10 dup(' ')
n=$-S1
k=n+n
s2 db k dup(' ')
data ENDS

code SEGMENT
assume cs:code, ds:data
begin:
mov ax, data
mov ds, ax

mov ah,3fh
mov bx,0
mov cx,10
lea dx,s1
int 21h

sub ax,2

mov si,0
mov di,0

l4:
mov bl,s1[si]
mov s2[di],bl
inc di
inc si

mov bl,s1[si]
mov s2[di],bl
inc di
mov s2[di],bl
inc di
inc si
cmp si,ax
jne l4


mov ah,40h
mov bx,1
mov cx,n
lea dx,s2
int 21h

mov ah,7
int 21h

mov ax, 4c00h
int 21h
code ENDS
END begin
