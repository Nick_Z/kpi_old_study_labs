STACK SEGMENT STACK
DB 100 dup (?)
STACK ENDS
				
 DATA SEGMENT
 DB	8 dup ('abcdefgh')
	   61 62 63 64 65	
	   66 67 68		
			 ]	
				
 M1		DB	0
					
 DATA ENDS
CODE SEGMENT
ASSUME CS:CODE, SS:STACK, DS:DATA
				
PRMAUS PROC FAR				
							;procedure of pulling m
				ouse's button
 PUSH	DS    
 PUSH	ES
 INC		M1
 CMP		M1,8
 JB		f2
MOV		M1,0
					 		
f2:		MOV		AL,M1
 MOV		AH,5
 INT		10H	
						
 POP		ES
 POP		DS
						
ret
						
PRMAUS ENDP
				
BEGIN:
MOV		AH,00H
MOV		AL,03H
MOV		BH,00H
INT		10H	
				;enable
				 videomode 
					
MOV		AX,0B800H
								;physical adres
				s of video memory
MOV		ES,AX
						
MOV		AX,DATA
MOV		DS,AX
XOR		DX,DX
MOV		AL,00010100b
			;start atributes
MOV		BX,0B800h
XOR     AH,AH
JMP		L2
				
L1:		ADD	BX,100h	
							;next videopage
MOV		ES,BX
INC		DX
						 
L2:		MOV	CX,2000
ADD		AL,00010001b
							;change atributes for n
				ext page
XOR		DI,DI
						
				
PRINT:					
								;put symbols on
				 videopage
 MOV		SI,DX
 MOVSB			
								;put data from 
								ds:si to es:di
 STOSB			
								;put symbol to 
								videopage 
										
 LOOP	PRINT
 INC		AH
 CMP		AH,8
 JL		L1
						
 XOR		SI,SI
 XOR		DI,DI
						
 XOR		AX,AX
 INT		33H
					
 XOR		CX,CX
 INT		33H
							
						
 XOR		AH,AH
						
						
 MOV		AX,0CH

MOV		CX,10b
PUSH	ES	
PUSH	CS	
POP		ES
LEA		DX,PRMAUS
INT		33H
						
@10:
jmp		@10
						
MOV		AH,01H
INT		21H
				
CODE	ENDS
END	main
