.386

data 	SEGMENT	use16

M	dd -5, 10, 7, -2, 15, 8
n = 6
k	db 2

data ends


code 	SEGMENT	use16
	assume 		cs:code, ds:data

begin:

mov 	ax, data
mov	ds, ax

mov 	ecx, n-2
mov	ebx, M
mov	edx, n-1
mov	eax, M[edx*4]
mov	edx, 1
	
L1:

mov	esi, M[edx*4]
cmp	esi, ebx
jnge	L2

cmp	esi, eax
jnle	L2

add k, 1

L2:

add edx, 1

loop L1

mov	ax, 4c00h
int	21h

code 	ends

end 	begin