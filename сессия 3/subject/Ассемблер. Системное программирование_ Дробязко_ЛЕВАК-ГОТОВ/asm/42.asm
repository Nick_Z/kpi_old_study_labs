data segment
string db "HelloWolrd", 10, '$'
null db 13,10
field_in db 10 dup(' ')
data ends
code segment
	assume cs:code, ds:data
	include macro.txt
begin:
	mov ax, data
	mov ds, ax

	outputstr string
	outputchar null, 2
	outputchar string+1, 1
	outputchar null, 2
	outputchar string+2, 1
	outputchar null, 2
	inputstr field_in
	outputstr string
	outputchar null, 1
	outputstr field_in
mov ah, 7
int 21h
mov ax, 4c00h
int 21h
code ends
end begin