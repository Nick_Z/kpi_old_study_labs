.386 
Data1 segment use16 
			I1 db ? 
 			I2 db ? 
			I3 db ? 
A1 dw 6 dup (8 dup (7 dup (0))) 
	
Data1 ends 
Code1 segment use16 
 ASSUME cs:Code1, ds:Data1 
ddr_tocode2 dw tocode2 
Main: 
		mov ax,Data1 
		mov ds,ax 
	    mov i1,5 
Label1: 
	    mov i2,7 
Label2: 
		mov i3,6 
Label3: 
		xor ax,ax 
	    mov al,i1 
	    imul i2 
		xor bx,bx 
		mov bl,i3 
		imul bx 
		mov bl,i1 
		mov si,bx	  
	    imul si,8 
		mov bl,i2 
	    add si,bx	 
		imul si,7 
        mov bl,i3 
        add si,bx 
		shl si,1	  
		mov A1[si],ax 
 		dec i3 
	    cmp i3,0 
		jne Label3 
 	   	dec i2 
	   	cmp i2,0 
		jne Label2 
	  	dec i1 
	 	cmp i1,0 
		jne Label1 
	 	nop 
		jmp far ptr tocode2 

Code1 Ends  
Data2 segment use16 
	A2 dw 6 dup (8 dup (7 dup (0))) 
Data2 ends 
				 
Code2 segment use16 
 ASSUME cs:Code2, ds:Data1, es:Data2 
 tocode2 label far 
 		mov ax,Data2 
 		mov ds,ax 
 		mov ax,Data1 
 		mov es,ax 
 		cld 
 	    mov cx, 336 
 		mov ax, 36 
 		lea di, A1 
Label4: 
 	    repnz scasw 
 		cmp cx, 0 
 		jz exit 
 		lea bx, [di]-2 
        mov word ptr ds:[A2], bx 
        jmp Label4 
exit: 
		mov	ax, 4c00h	 
	    int	21h 
Code2 Ends 
end main  
 

