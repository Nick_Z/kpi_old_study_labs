﻿/* Amo_Lab3
 * Zarivnyak O.I KV-12
 * 06.11.2013
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AMO_Lab3
{
    class Program
    {
        const int N = 4;
        const double eps = 1e-16;

        static void printMatrix(double [,] matrix, int n, int m)
        {
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                    Console.Write(matrix[i, j] + "\t");
                Console.WriteLine();
            }
        }

        static bool methodGaussaGordana(double[,] matrix, int n, int m)
        {
            bool f = true;
            for (int i = 0; i < n; i++)
            {
                for (int j = i + 1; j <= m; j++)
                    if (matrix[i, i] == 0.0)
                        f = false;
                    else
                    {
                        matrix[i, j] /= matrix[i, i];
                        f = true;
                    }
                matrix[i, i] = 1;

                for (int k = 0; k < n; k++)
                {
                    for (int j = i + 1; j <= m; j++)
                        if (k != i)
                            matrix[k, j] -= matrix[i, j] * matrix[k, i];
                        else
                            f = false;
                    if (k != i)
                        matrix[k, i] = 0;
                }
            }
            return f;
        }
        static public double Norma(double[,] matrix)
        {
            double max = 0, temp;
            int n = (int)Math.Sqrt(matrix.Length);
            for (int j = 0; j < n; j++)
                max += Math.Abs(matrix[0, j]);
            for (int i = 1; i < n; i++)
            {
                temp = 0;
                for (int j = 0; j < n; j++)
                    temp += Math.Abs(matrix[i, j]);
                if (temp > max)
                    max = temp;
            }
            return max;
        }

        static void methodIterativeSeidela(double[,] matrix, double[] mas, int n)
        {
            double[] X = new double[n] ;
            double[] XN = new double[] { 0, 0, 0, 0 };
            double norma = 0.0;
            double[,] a = new double[n, n];
            for (int i = 0; i < n; i++)
            {
               mas[i] = mas[i] / matrix[i, i];
               for (int j = 0; j < n; j++)
                   if (i != j)
                     a[i, j] = -matrix[i, j] / matrix[i, i];
                   else
                     a[i, j] = 0;
                X[i] = mas[i];
            }
            double q = Norma(a);
            if (q >= 1)
                Console.WriteLine("q > 1!");
            norma = Math.Abs(X[0]);
            for (int i = 1; i < n; i++)
                if (Math.Abs(X[i]) > norma)
                    norma = Math.Abs(X[i]);
            while(norma > (1 - q)* eps / q ){
                for (int i = 0; i < n; i++)
                {
                    X[i] = mas[i];
                    for (int j = 0; j < n; j++)
                        X[i] += a[i, j] * X[j];
                    
                }for (int j = 0; j < n; j++)
                    {
                        norma = 0.0;
                        if (Math.Abs(X[j] - XN[j]) > norma)
                            norma = Math.Abs(X[j] - XN[j]);
                        XN[j] = X[j];
                    }
            }
            for (int i = 0; i < n; i++)
                Console.WriteLine("x[{0}] = {1}", i, X[i]);   
        }

        static void Main(string[] args)
        {
            double[,] A = new double[,]{
                              {3, 7, 6, 0, 63},
                              {13, 49, 20, 15, 353},
                              {20, 19, 50, 10, 437},
                              {17, 7, 17, 17, 225}};
            printMatrix(A, N, N + 1);
            bool f = methodGaussaGordana(A, N, N);
            
                Console.WriteLine("-----Method Gaussa-Gordana----");
                printMatrix(A, N, N + 1);
            
            double[,] A1 = new double[,]{
                              {72, 3, 3, 9},
                              {13, 49, 20, 15},
                              {20, 19, 50, 10},
                              {3, -2, 10, 25}};
            double[] B = new double[] { 207, 353, 437, 160 };
            Console.WriteLine("-----Method iterative Seidela (Gaussa-Seidela)----");
            methodIterativeSeidela(A1, B, N);
        }
    }
}
