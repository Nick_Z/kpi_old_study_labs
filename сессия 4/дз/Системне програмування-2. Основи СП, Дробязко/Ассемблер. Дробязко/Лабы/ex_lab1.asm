.386
Data 		Segment use16
Vb 		db 		10011b
String		db		"String � new"
Vw 		dw 		4567d
Vd 		dd 		0d7856fdah
Doublesg	dw 		678
QWERTY	dd		67ff89h
Zxcv		db		89h
Data 		ends
Code 		Segment use16
Assume 	cs:Code, Ds:Data
label1:
Cli
Inc 		cl
Inc 		al
Jb		Label2
Inc 		Bx
Inc 		Ax
Inc 		Ebx
Inc 		Eax
Dec 		Vw[Si]
Dec 		es:Vw[Si]
Dec		gs:zxCV[Bp]
Dec		zxCV[Bx]
Add 		Eax, Ecx
Add 		Ax, Cx
Add 		Al, Cl
Cmp 		Bx, Doublesg[Edi]
Cmp		Ebx, qwerty[Ebx]
Cmp		Bl, String[Ebx]
Xor 		vb[Edx], Al
Mov 		Dx, 5634h
Mov 		Edx, 34
Or 		Vd[esp], 0101b
Jb		label1
Label2:
Code 		ends
			End  label1
