; ���� ������� �� ������ ������� ������ �������� ������
.386
Data Segment  
text db 12*80*2  dup (?)
Data  endS

Code segment USE16
	assume cs:Code,ds:Data

MouseP proc far
        	push es
	push ds
	pushA

	test bl,00000001b
	jz exit_mouseP

	mov ax,2 	;hide mouse cursor
	int 33h
                
	cld
	mov ax,Data
	mov es,ax
                mov ax,0B800h
	mov ds,ax

	mov di,offset text
	xor si,si
	mov cx,12*80
	rep movsw

	mov ax,0B800h
	mov es,ax
	mov si,12*80*2
	xor di,di
	mov cx,12*80
	rep movsw

	mov ax,Data
	mov ds,ax
                mov ax,0B800h
	mov es,ax
	mov si,offset text
	mov di,12*80*2
	mov cx,12*80
	rep movsw

	mov ax,1 	;show mouse cursor
	int 33h
exit_mouseP:
	popA
	pop ds
	pop es

	retF
MouseP endP

Entry_Point: 
                mov ax,Data
	mov ds,ax

                mov ax, 3 	;set text mode
                int 10h   		;25-lines 80-columnes
                		
                mov ax,0B800h
	mov es,ax

	cld
	mov ax,'A'+256*(16+15)
	mov cx,80*12
	xor di,di
	rep stosw

	mov ax,'B'+256*(32+15)
	mov cx,80*12
	rep stosw
        
	xor ax,ax 	;init mouse
	int 33h
        
	mov ax,1 	;show mouse cursor
	int 33h

                mov dx,offset MouseP
	mov ax,cs
	mov es,ax
	mov cx,000000010b
	mov ax,0Ch
	int 33h
		
	mov ah, 01h	
	int   21h
		
	mov ax,2 	;hide mouse cursor
	int 33h

	xor cx,cx	
	mov ax,0ch		
	int 33h		
                
	mov ax,4c00h
	int 21h
                
Code endS
end Entry_Point