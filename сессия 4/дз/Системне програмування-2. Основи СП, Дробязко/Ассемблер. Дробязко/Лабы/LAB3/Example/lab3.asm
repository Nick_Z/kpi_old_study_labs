.386

MX equ 320
MY equ 200

data segment use16
;X
	min_x dq -4.0 ; мінімальне значення по осі х
	max_x dq 4.0 ; максимальне значення по осі х
	max_crt_x dw MX ; максимальна кількість точок на екрані по осі х
	crt_x dw ? ; екранна координата по осі х
	scale_x dq ? ; масштаб по осі х
;Y
	min_y dq -2.2
	max_y dq 2.2
	max_crt_y dw MY
	crt_y dw ?
	scale_y dq ?
	zr dq 0
data ends

scale macro p1 
	fld max_&p1 ; st0=max_&p1; top=7
	fsub min_&p1 ; st0=max_&p1 - min_&p1; ;top=7
	fild max_crt_&p1 ; st0=max_crt_&p1,
				; st1=max_&p1-min_&p1; top=6
	fdivp st (1), st (0) ; 1-й крок st1=st1/st0
				; 2-й крок st1 стає st0 top=7
				; і містить масштаб
	fstp scale_&p1 ; top=0
endm 

real macro p1
	fld scale_&p1 ; st0 - масштаб
	fild crt_&p1 ; st0=crt_&p1, st1-масштаб
			;top=6
	fmulp st (1), st (0) ; top=7
	fadd min_&p1 ; st0 - реальне зн. Х top=7 
endm

screen macro p1
	; контроль діапазону (top не змінюється)
	fcom min_&p1; порівняння ST (0) та min_&p1
	fstsw ax; результат порівняння в ax
	sahf ; результат порівняння
		;ST (0) та min_y в регістр Flags
	jc @minus ; st0 < min_&p1
		; поза видимим діапазоном
		; по @minus забезпечити top=0 і
		; crt_y=max_crt_y
	fcom max_&p1 ; порівняння ST (0) та max_&p1
	fstsw ax
	sahf
	ja @plus ; st0 > max_&p1 (zf=cf=0)
		; поза видимим діапазоном
		; по @plus - забезпечити top=0
		; і встановити crt_y=0
	fsub min_&p1;
	fdiv scale_&p1
	frndint ; округлення до цілого
	fistp crt_&p1 ; TOP=0!!!
	mov si,max_crt_&p1 ;CHANGED, prev - ax
	sub si,crt_&p1
	mov crt_&p1,si ; дзеркальне відображення 
	jmp @end
	@plus:
		fistp crt_&p1
		;push max_crt_&p1
		;pop crt_&p1
		;mov si, crt_&p1
		jmp @loop
	@minus:
		fistp crt_&p1
		;mov crt_&p1,0
		;xor si,si
		jmp @loop
	@end:
endm

assume cs:code,ds:data

code segment use16
Osi PROC
    PUSHA
    MOV CX, 0       ;начало горизонтальной
    MOV DX, 100         ;оси
    MOV AL, 00000110b   ;цвет оси желтый
o1: MOV AH, 12      ;вывод точки        
    INT 10h         ;вызов BIOS
    INC CX          ;построить 
    CMP CX, 320     ;300
    JNE o1          ;точек
;---------------------------------------------------------
    MOV CX, 160          ;начало вертикальной
    MOV DX, 0        ;оси
    MOV AL, 00000110b    ;цвет оси желтый
o2: MOV AH, 12       ;вывод точки
    INT 10h
    INC DX          
    CMP DX, 200
    JNE o2
    POPA
    RET
Osi ENDP
begin:

	push data 
	pop ds
	
	push 0a000h 
	pop es
     	
	mov ax, 0013h 	;init video 320x200 graph
	int 10h 

	finit ;init math
	
	scale x ;form scale_x
	scale y ;form scale_y

	mov bl,01000111b ; set color
	call Osi
	
	mov cx,MX
	dec cx
	loop2:
		mov crt_x,cx
		real x 		;st(0) = x
		fptan 		;st(0) = 1.0
				;st(1) = tan(x)
		;fdivp st(1),st(0)		;st(0) = 1/tan(x) = ctan(x) = y
		fistp crt_x
		screen y 	;si = y
		;and si, 01ffh
		;jnz @loop
		;cmp si,200
		;jna @loop
		mov ax,MX 	;get offset
		mul si	
		add ax,cx
		mov si,ax
		mov es:[si],bl 	;draw point
		@loop:
		loop loop2
	
	mov ah, 8	;wait for key
	int 21h
	
	mov 	ax, 3	;init video, clear screen
	int 	10h
	
	mov ax,	4c00h	;exit
	int 21h

code ends
end begin
