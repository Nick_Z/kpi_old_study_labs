#include <iostream>
#include <fstream>
#include <cstdlib>

using namespace std;

void writeToFile(char *fileName, int *content, int dataNumber) {
    char *str;
    char buffer[16] = {0};

    FILE * file = fopen(fileName, "w");

    if (file) {
         for (int i = 0; i < dataNumber; i++) {
            sprintf(buffer, "%d", content[i]);
            fputs(buffer, file);
            fputs(" ", file);
        }

    } else {
        cout << "Cannot read file" << endl;
    }

    fclose(file);
    return;
}

int* readFromFile(char *fileName, int dataNumber) {
    ifstream instr(fileName);
    int *content = new int[dataNumber];

     for (int i = 0; i < dataNumber; i++) {
            instr >> content[i];
    }

    instr.close();

    return content;
}

int main()
{
    int *content, *fromFile;
    int n, rotdist;
    char *fileName = "f.txt";
    setlocale(0, "");

    cout << "Insert number:" << endl;
    cin >> n;

    if (cin.fail() || n <= 0) {
        cout << "Try valid integer next time" << endl;
        return 0;
    }

    cin.clear();

    content = new int[n];

    for (int k = 0; k < n; k++) {
        content[k] = rand() % 100;
    }

    writeToFile(fileName, content, n);

    cout << "Your file now is:" << endl;
    for (int i = 0; i < n; i++) {
        cout << readFromFile(fileName, n)[i] << " ";
    }

    cout << " " << endl;
    cout << "Insert a valid number parts for replacing:" << endl;
    cin >> rotdist;

    while (cin.fail() || rotdist > n || rotdist <= 0) {
        cout << "Number is not valid" << endl;
        cin.clear();
        cin.ignore(256, '\n');
        cin >> rotdist;
    };

    if (!readFromFile(fileName, n)[0]) {
        cout << "File is empty" << endl;
        return 0;
    }

    // Let's solve the case
    //

    fromFile = readFromFile(fileName, n);

    int temp;
    for (int j = 0; j < rotdist; j++){
        temp = fromFile[0];
        for (int i = 0; i < n-1; i++){
            fromFile[i] = fromFile[i+1];
        }

        fromFile[n-1] = temp;
    }

    writeToFile(fileName, fromFile, n);

    return 0;
}
