#include <iostream>
#include <cmath>
#include <fstream>


using namespace std;

void writeToFile(char *fileName, int *content, int dataNumber) {
    char *str;
    char buffer[16] = {0};

    FILE * file = fopen(fileName, "w");

    if (file) {
         for (int i = 0; i < dataNumber; i++) {
            sprintf(buffer, "%d", content[i]);
            fputs(buffer, file);
            fputs("\n\r", file);
        }

    } else {
        cout << "Cannot read file" << endl;
    }

    fclose(file);
    return;
}

int* readFromFile(char *fileName, int dataNumber) {
    ifstream instr(fileName);
    int *content = new int[dataNumber];

     for (int i = 0; i < dataNumber; i++) {
            instr >> content[i];
    }

    instr.close();

    return content;
}

int main()
{
    int *content, *contentFromFile, *validContentToWrite;
    int n;
    char *fileNameFirst = "f.txt";
    char *fileNameSecond = "g.txt";
    setlocale(0, "");

    cout << "Insert number:" << endl;
    cin >> n;

    if (cin.fail()) {
        cout << "Try integer next time" << endl;
        return 0;
    }

    content = new int[n];

    content[0] = pow(2,(1+1)) - log(1);

    for (int k = 1; k < n; k++) {
        content[k] = content[k-1]*(pow(2,(k+1)) - log(k+1));
    }

    writeToFile(fileNameFirst, content, n);

    contentFromFile = readFromFile(fileNameFirst, n);

    // let's validate
    //

    int counter = 0;
    for (int i = 0; i < n; i++) {
        if (contentFromFile[i] <= 1000) {
            counter++;
        }
    }

    validContentToWrite = new int[counter];
    for (int i = 0; i < n; i++) {
        if (contentFromFile[i] <= 1000) {
            validContentToWrite[i] = contentFromFile[i];
        }
    }

    writeToFile(fileNameSecond, validContentToWrite, counter);

    return 0;
}
