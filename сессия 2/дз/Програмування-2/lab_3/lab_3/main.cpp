#include <iostream>
#include <fstream>
#include <string>
#include <vector>

using namespace std;

int main()
{
        string fileOne = "f.txt", fileTwo = "g.txt";
        vector <string> v1,v2;
        ifstream file;
        ofstream outFile;
        int i;



        file.open(fileOne.c_str(), ifstream::in);
        while(getline(file, fileOne))
        {
            v1.push_back(fileOne);
        }
        file.close();

        file.open(fileTwo.c_str(), ifstream::in);
        while(getline(file, fileTwo))
        {
            v2.push_back(fileTwo);
        }
        file.close();

        outFile.open ("h.txt");

        cout << "Different strings:" << endl;
        int noCommonFlag = 1;
        for(i = 0; i < v1.size(); i++) {
            if(v1[i] == v2[i]) {
                outFile << v1[i];
                outFile << '\n';

                noCommonFlag = 0;
            } else {
                cout << v1[i] << endl;
                cout << "--" << endl;
                cout << v2[i] << endl;
            }

        }

        if (noCommonFlag) {
            cout << "!!!" << endl;
            cout << "There are no common lines" << endl;
        }

        outFile.close();

        return 0;
}
