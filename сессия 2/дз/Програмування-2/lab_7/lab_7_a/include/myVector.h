#include<iostream>
#include<vector>

using namespace std;

#ifndef _myVector
#define _myVector
class myVector{
    public:
        int getVectorSize();
        int setVectorSize(int number);
        void fillVector(int number);
        void setId(int id);
        void showVector(int filter = 0);
        int getId();

    private:
        int id;
        int size;
        vector<int> vectorBody;
};
#endif
