#include <iostream>
#include<conio.h>
#include<myVector.h>
using namespace std;

int main()
{
    int vectorId, vectorSize, vectorSizeFilter;
    char showId, showSize, showVector, showVectorWithFilter;

    myVector vetcorEntity;

    cout << "Enter vector id :";
    cin >> vectorId;

    vetcorEntity.setId(vectorId);

    cout << "Enter vector size :";
    cin >> vectorSize;

    vetcorEntity.setVectorSize(vectorSize);

    cout << "Do you want to get vector id?(y/n)";
    cin >> showId;
    if (showId == 'y') {
        cout << "vector id is :"<< endl;
        cout << vetcorEntity.getId() << endl;
    }

    cout << "Do you want to get vector size?(y/n)";
    cin >> showSize;
    if (showSize == 'y') {
        cout << "vector size is:"<< endl;
        cout << vetcorEntity.getVectorSize() << endl;
    }

    cout << "Do you want to show vector?(y/n)";
    cin >> showVector;
    if (showVector == 'y') {
        cout << "vector is:"<< endl;
        vetcorEntity.showVector();
    }

    cout << "Do insert size filter and show vector?(y/n)";
    cin >> showVectorWithFilter;
    if (showVectorWithFilter == 'y') {
        cout << "Insert start size:";
        cin >> vectorSizeFilter;
        vetcorEntity.showVector(vectorSizeFilter);
    }

    return 0;
}
