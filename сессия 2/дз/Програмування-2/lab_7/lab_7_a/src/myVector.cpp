#include <myVector.h>
#include<vector>
#include <cstdlib>
#include <iostream>

using namespace std;

int myVector::getVectorSize() {
    return this->vectorBody.size();
};

int myVector::setVectorSize(int number) {
    this->fillVector(number);
    return this->size = number;
};

int myVector::getId(){
    return this->id;
};

void myVector::setId(int id) {
    this->id = id;
};

void myVector::fillVector(int number) {
    for(int i = 1; i <= number; i++){
        this->vectorBody.push_back(i + rand());
    }
};

void myVector::showVector(int filter) {
    for (int i = filter; i < this->vectorBody.size(); i++) {
        cout << this->vectorBody[i] << " ";
    }

    cout << " " << endl;
};
