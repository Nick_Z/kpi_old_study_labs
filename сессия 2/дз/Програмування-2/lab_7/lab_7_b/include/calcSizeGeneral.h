#include <lineInterface.h>

using namespace std;

#ifndef _calcSizeGeneral
#define _calcSizeGeneral

class calcSizeGeneral: public lineInterface
{
    public:
        virtual void calculateLineSize();
    protected:
    private:
};

#endif
