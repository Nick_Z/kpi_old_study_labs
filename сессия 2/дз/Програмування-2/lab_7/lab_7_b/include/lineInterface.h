#include <iostream>
#include <dot.h>

using namespace std;

#ifndef _lineInterface
#define _lineInterface

class lineInterface{
    public:
        virtual ~lineInterface() {}
        virtual void calculateLineSize() = 0;

        void setFirstDot(dot dot){ this->firstDot = dot; };
        dot getFirstDot(){ return this->firstDot; };

        void setSecondDot(dot dot){ this->secondDot = dot; };
        dot getSecondDot(){ return this->secondDot; };

        int isCoordinateEqual () {
            if (this->firstDot.getCoordinate('x') == this->secondDot.getCoordinate('x')
                && this->firstDot.getCoordinate('y') == this->secondDot.getCoordinate('y')
            ){
                cout << "Dots has an equal coordinates" << endl;
                return 1;
            }

            return 0;
        }
    protected:
        dot firstDot;
        dot secondDot;
};
#endif
