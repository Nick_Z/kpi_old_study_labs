
using namespace std;

#ifndef _dot
#define _dot

class dot{
    public:
        void setCoordinate(char coordinateName, int value);
        int getCoordinate(char coordinateName);
    private:
        int coordinateX;
        int coordinateY;
};
#endif
