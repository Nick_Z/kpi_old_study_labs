#include <lineInterface.h>

using namespace std;

#ifndef _calcSizeByFormula
#define _calcSizeByFormula

class calcSizeByFormula: public lineInterface
{
    public:
        virtual void calculateLineSize();
    protected:
    private:
};

#endif
