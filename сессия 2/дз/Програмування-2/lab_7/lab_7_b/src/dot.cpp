#include <cstdlib>
#include <iostream>

#include <dot.h>

using namespace std;

void dot::setCoordinate(char coordinateName, int value) {
    if  (coordinateName == 'x') {
        this->coordinateX = value;
    }

    if  (coordinateName == 'y') {
        this->coordinateY = value;
    }
};

int dot::getCoordinate(char coordinateName) {
    if  (coordinateName == 'x') {
        return this->coordinateX;
    }

    if  (coordinateName == 'y') {
        return this->coordinateY;
    }
};
