#include <cstdlib>
#include <iostream>

#include <calcSizeByFormula.h>

using namespace std;

void calcSizeByFormula::calculateLineSize() {
     if (this->isCoordinateEqual()) {
        return;
    }

    float k = (this->firstDot.getCoordinate('y') - this->secondDot.getCoordinate('y'))
            / (this->firstDot.getCoordinate('x') - this->secondDot.getCoordinate('x'));

	float b = this->secondDot.getCoordinate('y') - k * this->firstDot.getCoordinate('x');

    cout << "The equation of the line (y=kx+b) is:" << endl;
    cout << "y=";
    if (k != 0 ) {
      cout << k;
    }

    cout << "x";
    if(b != 0) {
        cout << "-" << b;
    }
    cout << " " << endl;

};
