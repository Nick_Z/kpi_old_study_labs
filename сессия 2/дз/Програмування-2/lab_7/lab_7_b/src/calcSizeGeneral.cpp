#include <iostream>
#include <cmath>

#include <calcSizeGeneral.h>

using namespace std;

void calcSizeGeneral::calculateLineSize() {
    if (this->isCoordinateEqual()) {
        return;
    }


    float diffY = this->firstDot.getCoordinate('y') - this->secondDot.getCoordinate('y');
    float diffX = this->firstDot.getCoordinate('x') - this->secondDot.getCoordinate('x');

    cout << "The line size is:" << endl;
    cout << sqrt((diffY * diffY) + (diffX * diffX)) << endl;
};
