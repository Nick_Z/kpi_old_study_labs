#include <iostream>

#include <dot.h>
#include <calcSizeByFormula.h>
#include <calcSizeGeneral.h>

using namespace std;

int main()
{
    int firstDotX, firstDotY, secondDotX, secondDotY;
    calcSizeGeneral sizeGeneral;
    calcSizeByFormula sizeByFormula;
    dot dot;

    cout << "Insert first dot X:" << endl;
    cin >> firstDotX;
    if (cin.fail()) {
        cout << "Try integer next time";
        return 0;
    }

    cout << "Insert first dot Y:" << endl;
    cin >> firstDotY;
    if (cin.fail()) {
        cout << "Try integer next time";
        return 0;
    }

    cout << "Insert second dot X:" << endl;
    cin >> secondDotX;
    if (cin.fail()) {
        cout << "Try integer next time";
        return 0;
    }

    cout << "Insert second dot Y:" << endl;
    cin >> secondDotY;
    if (cin.fail()) {
        cout << "Try integer next time";
        return 0;
    }

    dot.setCoordinate('x', firstDotX);
    dot.setCoordinate('y', firstDotY);

    sizeGeneral.setFirstDot(dot);
    sizeByFormula.setFirstDot(dot);

    dot.setCoordinate('x', secondDotX);
    dot.setCoordinate('y', secondDotY);

    sizeGeneral.setSecondDot(dot);
    sizeByFormula.setSecondDot(dot);

    sizeGeneral.calculateLineSize();
    sizeByFormula.calculateLineSize();

    return 0;
}
