#include <graphics.h>
#include <conio.h>
#include <math.h>
#include <iostream>

using namespace std;

int main() {
    float x, y;
    int a;
    int gdriver = DETECT, gmode;
    initgraph(&gdriver, &gmode, "");

    setcolor(WHITE);
    outtextxy(10, 10, "Created by Zviahintsev Mykola");

    setcolor(WHITE);
    outtextxy(10, 300, "Strofoid");

    setcolor(WHITE);
    outtextxy(10, 330, "y = a*t*(t^2 - 1)/(t^2 + 1)");
    setcolor(WHITE);
    outtextxy(10, 360, "x = a*(t^2 - 1)/(t^2 + 1)");

    // X and Y
    //
    setcolor(WHITE);
    outtextxy(320, 5, "Y");
    setcolor(WHITE);
    outtextxy(600, 240, "X");



    // y
    //
    line(311, 0, 311, 480);

    line(320, 30, 300, 30);
    line(320, 60, 300, 60);
    line(320, 90, 300, 90);
    line(320, 120, 300, 120);
    line(320, 150, 300, 150);
    line(320, 180, 300, 180);
    line(320, 210, 300, 210);

    line(320, 255, 300, 255);
    line(320, 285, 300, 285);
    line(320, 315, 300, 315);
    line(320, 345, 300, 345);
    line(320, 375, 300, 375);
    line(320, 405, 300, 405);
    line(320, 435, 300, 435);

    setcolor(WHITE);
    outtextxy(320, 200, "1");
    setcolor(WHITE);
    outtextxy(320, 140, "3");
    setcolor(WHITE);
    outtextxy(320, 80, "5");
    setcolor(WHITE);
    outtextxy(320, 20, "7");
    setcolor(WHITE);
    outtextxy(320, 250, "-1");
    setcolor(WHITE);
    outtextxy(320, 310, "-3");
    setcolor(WHITE);
    outtextxy(320, 370, "-5");
    setcolor(WHITE);
    outtextxy(320, 430, "-7");
    setcolor(WHITE);
    outtextxy(320, 490, "-9");

    // x
    //
    line(0, 231, 940, 231);

    line(40, 219, 40, 244);
    line(70, 219, 70, 244);
    line(100, 219, 100, 244);
    line(130, 219, 130, 244);
    line(160, 219, 160, 244);
    line(190, 219, 190, 244);
    line(220, 219, 220, 244);
    line(250, 219, 250, 244);
    line(280, 219, 280, 244);

    line(340, 219, 340, 244);
    line(370, 219, 370, 244);
    line(400, 219, 400, 244);
    line(430, 219, 430, 244);
    line(460, 219, 460, 244);
    line(490, 219, 490, 244);
    line(520, 219, 520, 244);
    line(550, 219, 550, 244);
    line(580, 219, 580, 244);
    line(610, 219, 610, 244);
    line(640, 219, 640, 244);

    setcolor(WHITE);
    outtextxy(340, 235, "1");
    setcolor(WHITE);
    outtextxy(400, 235, "3");
    setcolor(WHITE);
    outtextxy(460, 235, "5");
    setcolor(WHITE);
    outtextxy(520, 235, "7");
    setcolor(WHITE);
    outtextxy(580, 235, "9");

    setcolor(WHITE);
    outtextxy(270, 235, "-1");
    setcolor(WHITE);
    outtextxy(210, 235, "-3");
    setcolor(WHITE);
    outtextxy(150, 235, "-5");
    setcolor(WHITE);
    outtextxy(90, 235, "-7");
    setcolor(WHITE);
    outtextxy(30, 235, "-9");

    // start from X, Y
    //
    moveto(1000, 1000);

    a = 100;
    for(double t = -100.00; t < 100.0; t = t + 0.01) {
        y = a*t*((pow(t, 2) - 1))/(pow(t, 2) + 1);
        x = a*((pow(t, 2) - 1))/(pow(t, 2) + 1);

        lineto(x+getmaxx()/2, getmaxy()/2-y);
    }

    getch();
    closegraph();
    return 0;
}
