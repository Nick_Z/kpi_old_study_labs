#include <iostream>
#include <set>
#include <cstdlib>
#include <iterator>
#include <algorithm>

using namespace std;

void printSet(set<char> setName, string name) {
    cout << "Set " << name << " :";
    copy( setName.begin(), setName.end(), ostream_iterator<int>(cout, " "));
    cout << "\n\r";
    cout << "-------------";
    cout << "\n\r";
}

void assembleOne(set<char> A, set<char> B, set<char> C, set<char> D, set<char> E) {
    set<char> diffAB,
            diffBA,
            diffMerge,
            intersectDiffMergeWithC,
            intersectDiffMergeWithCWithD,
            resultSet;

    printSet(A, "A");
    printSet(B, "B");
    printSet(C, "C");
    printSet(D, "D");
    printSet(E, "E");

    set_difference(A.begin(),
                A.end(),
                B.begin(),
                B.end(),
                inserter(diffAB, diffAB.begin()));

    set_difference(B.begin(),
                   B.end(),
                   A.begin(),
                   A.end(),
                   inserter(diffBA, diffBA.begin()));

    diffMerge.insert(diffAB.begin(), diffAB.end());
    diffMerge.insert(diffBA.begin(), diffBA.end());

    set_intersection(C.begin(),
                     C.end(),
                     diffMerge.begin(),
                     diffMerge.end(),
                     inserter(intersectDiffMergeWithC, intersectDiffMergeWithC.begin()));

    set_intersection(intersectDiffMergeWithC.begin(),
                     intersectDiffMergeWithC.end(),
                     D.begin(),
                     D.end(),
                     inserter(intersectDiffMergeWithCWithD, intersectDiffMergeWithCWithD.begin()));

    set_intersection(intersectDiffMergeWithCWithD.begin(),
                     intersectDiffMergeWithCWithD.end(),
                     E.begin(),
                     E.end(),
                     inserter(resultSet, resultSet.begin()));

    cout << "Result is: ";
    cout << "\n\r";

    if (resultSet.size()) {
        printSet(resultSet, "");
    } else {
        cout << "EMPTY";
    }
}

void assembleTwo(set<char> A, set<char> B, set<char> C, set<char> D, set<char> E) {
    set<char> intersectAB,
            intersectABwithC,
            diffIntersectABwithCfromD,
            resultSet;


    printSet(A, "A");
    printSet(B, "B");
    printSet(C, "C");
    printSet(D, "D");
    printSet(E, "E");


    set_intersection(A.begin(),
                     A.end(),
                     B.begin(),
                     B.end(),
                     inserter(intersectAB, intersectAB.begin()));

    set_intersection(intersectAB.begin(),
                     intersectAB.end(),
                     C.begin(),
                     C.end(),
                     inserter(intersectABwithC, intersectABwithC.begin()));

    set_difference(intersectABwithC.begin(),
                   intersectABwithC.end(),
                   D.begin(),
                   D.end(),
                   inserter(diffIntersectABwithCfromD, diffIntersectABwithCfromD.begin()));

    set_difference(diffIntersectABwithCfromD.begin(),
                   diffIntersectABwithCfromD.end(),
                   E.begin(),
                   E.end(),
                   inserter(resultSet, resultSet.begin()));

    cout << "Result is: ";
    cout << "\n\r";

    if (resultSet.size()) {
        printSet(resultSet, "");
    } else {
        cout << "EMPTY";
    }
}



int main()
{
    char formula, random;
    set<char> A, B, C, D, E, mergeAE;

    cout << "Do you want to try random sets ? (y/n)" << endl;

    cin >> random;

    if (random == 'y') {
        cout << "Random sets:";
        cout << "\n\r";
        cout << "-------------";
        cout << "\n\r";
        for (int i = 0; i < 1000; i++) {
            A.insert(rand() % rand() + rand() % 101);
            B.insert(rand() % rand() + rand() % 102);
            C.insert(rand() % rand() + rand() % 103);
            D.insert(rand() % rand() + rand() % 104);
            E.insert(rand() % rand() + rand() % 105);
        }
    } else {
        cout << "Ok. So, what formula do you want to solve? (1 or 2)" << endl;

        cin >> formula;



        if (formula == '1') {
            cout << "Static sets to satisfy 1-st condition";
            cout << "\n\r";
            cout << "-------------";
            cout << "\n\r";
        } else {
            cout << "Static sets to satisfy 2-nd condition";
            cout << "\n\r";
            cout << "-------------";
            cout << "\n\r";


            C.insert(4);
            D.insert(112);
            E.insert(111);
        }

        A.insert(1);
        A.insert(2);
        A.insert(3);
        A.insert(4);
        A.insert(11);

        B.insert(3);
        B.insert(4);
        B.insert(5);
        B.insert(11);

        C.insert(5);
        C.insert(6);
        C.insert(7);
        C.insert(11);

        D.insert(5);
        D.insert(7);
        D.insert(8);
        D.insert(9);
        D.insert(11);

        E.insert(5);
        E.insert(6);
        E.insert(7);
        E.insert(11);
    }

        mergeAE.insert(A.begin(), A.end());
        mergeAE.insert(E.begin(), E.end());

        if((C == E) && includes(mergeAE.begin(), mergeAE.end(), B.begin(), B.end())) {
            if  (random == 'y') {
                cout << "Random values satisfied 1-st condition";
                cout << "\n\r";
                cout << "-------------";
                cout << "\n\r";
            }

            assembleOne(A, B, C, D, E);
        } else {
             if  (random == 'y') {
                cout << "Random values satisfied 2-nd condition";
                cout << "\n\r";
                cout << "-------------";
                cout << "\n\r";
            }

            assembleTwo(A, B, C, D, E);
        }

        return 0;
}
