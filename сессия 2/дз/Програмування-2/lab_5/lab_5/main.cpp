#include <iostream>
#include<stdio.h>
#include<math.h>
#include<conio.h>

// Accuracy
//
#define n 100

using namespace std;

// My data from the Varian sheet
//
const double a = 2.0, b = 3.0;
const double h = (b-a)/n;

double func[n+1];

// Here my function
//
double f(double x) {
    return sin(1/(x*(log(x)*log(x))));
}

double trapezium() {
    double result = 0.0;
    for(int i = 0; i < n; ++i) {
        result+=(func[i]+func[i+1])*h/2;
    }

    return result;
}

double simpson() {
    double result = 0.0;
    result +=(func[0]+func[n])*h/3;

    for(int i=1; i<n; i+=2) {
        result +=4*h/3*func[i];
    }

    for(int i=2; i<n; i+=2) {
        result +=2*h/3*func[i];
    }

    return result;
}

int main() {
    char check;
    for(int i = 0; i <= n; ++i) {
        func[i] = f(a+i*h);
    }

    cout << "Simpson's formula result is: \n\r"<< endl;
    cout << simpson() << "\n\r" << endl;

    cout << "Do you want to check with Trapezium formula? (y/n) \n\r"<< endl;
    cin >> check;

    if (check == 'y') {
        cout << "Trapezium formula result is: \n\r"<< endl;
        cout << trapezium() << endl;
        cout << "Pretty close, hah?" << endl;
    }

    return 0;
}
