#include "iostream"
#include "graphics.h"
#include "cstdlib"
#include "dos.h"
#include <cstdio>
using namespace std;

int main()
{
    initwindow(800,500);
    int x0,y0, i, j, k;
    int gd = DETECT,gm,ec;
    int xmax,ymax;

    ec=graphresult();

    setcolor(RED);
    rectangle(0, 0, getmaxx(), getmaxy());
    outtextxy(250, 240, "PRESS ANY KEY");

    while(!kbhit());

    for(i=50, j=0, k=0; i<=250, j<=250; i += 2, j += 2, k +=1) {
        delay(120);
        cleardevice();

        setfillstyle(1, YELLOW);
        fillellipse(i, 300-j, 20, 20);

        setfillstyle(1, LIGHTGREEN);
        fillellipse(400, 450, k/8, k/4);

        setcolor(DARKGRAY);
        setfillstyle(1, DARKGRAY);
        sector(400, 460-k/4, 0, 180, 7+k/8, 8+k/8);

        setcolor(BROWN);
        setfillstyle(1, BROWN);
        sector(400, 500, 30, 30, 250, 50);
    }

    setfillstyle(1, RED);
    fillellipse(250, 50, 20, 20);

    setfillstyle(1, BLACK);
    fillellipse(250, 50, 5, 5);

    setfillstyle(1, BLACK);
    fillellipse(300, 50, 5, 5);

    setcolor(WHITE);
    setfillstyle(1, WHITE);
    sector(270, 100, 180, 0, 20, 20);

    setfillstyle(1, LIGHTGREEN);
    fillellipse(440, 90, 100, 30);

    setfillstyle(1, LIGHTGREEN);
    sector(295, 100, 350, -330, 45, 45);

    setcolor(MAGENTA);
    settextstyle(5, 0, 2);
    setbkcolor(LIGHTGREEN);
    outtextxy(370, 80, "That's all Folks!");

    delay(10000);
    cleardevice();

    return 0;
}
