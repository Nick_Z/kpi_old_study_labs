//---------------------------------------------------------------------------

#include <conio.h>
#include <iostream>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctime>
#include <float.h>
#include <Windows.h>
#include <iomanip>
//---------------------------------------------------------------------------

using namespace std;

const int num = 201;
double a[num][num][num], buff[num][num][num];
int m, n, p;

double sum_arr(int);


void reset(double a1[num][num][num], double b1[num][num][num]) {
	for (int k = 0; k < p; k++)
		for (int i = 0; i < n; i++)
			for (int j = 0; j < m; j++)
				a1[i][j][k] = b1[i][j][k];
}


void inp_arr(int v, int v2, double an, double ak, double ahbig, double ahsmall) {
	double d1, d2, d3, a_tmp;
	srand(time(NULL));

	switch (v) {
	case (2): {
		for (int k = 0; k < p; k++) {
			a_tmp = an + k*ahbig;
			for (int i = 0; i < n; i++) {
				for (int j = 0; j < m; j++) {
					a[i][j][k] = a_tmp + (i + j)*ahsmall;
				}
			}
		}
		break;
	}
	case (1): {
		for (int k = 0; k < p; k++) {
			a_tmp = ak - k*ahbig;
			for (int i = 0; i < n; i++) {
				for (int j = 0; j < m; j++) {
					a[i][j][k] = a_tmp - (i + j)*ahsmall;
				}
			}
		}
		break;
	}
	case (3): {
		for (int k = 0; k < p; k++) {
			for (int i = 0; i < n; i++) {
				for (int j = 0; j < m; j++) {
					d1 = rand() % v2;
					d2 = 1 + rand() % 2;
					d3 = 1. - rand() % 10000 / 10000.;
					a[i][j][k] = pow(-1, d2)*pow(2., d1)*d3;
				}
			}
		}
		break;
	}
	default:
		cout << "Error!" << endl;
	}
}

double sum_arr(int k) {
	double sum = 0.;
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) {
			sum += a[i][j][k];
		}
	}
	return sum;
}

void insertSortP(double *pa, int p) {
	int j;
	double tmp;
	for (int i = 1; i < p; i++) {
		j = i;
		do {
			j--;
		} while (j >= 0 && pa[j] < pa[i] - DBL_EPSILON);

		tmp = pa[i];

		for (int k = i; k > j + 1; k--) {
			pa[k] = pa[k - 1];
		}
		pa[j + 1] = tmp;
	}
}

void insertInd(int *ind) {
	int j, tmp;
	for (int i = 1; i < p; i++) {
		j = i;
		do {
			j--;
		} while (j >= 0 && sum_arr(ind[j]) < sum_arr(ind[i]) - DBL_EPSILON);

		tmp = ind[i];

		for (int k = i; k > j + 1; k--) {
			ind[k] = ind[k - 1];
		}
		ind[j + 1] = tmp;
	}
}


void insertA3d(double a[num][num][num]) {
	int j;
	double tmp[num][num];
	for (int i = 1; i < n; i++) {
		j = i;
		do {
			j--;
		} while (j >= 0 && sum_arr(j) < sum_arr(i) - DBL_EPSILON);

		for (int l = 0; l < n; l++)
			for (int r = 0; r < m; r++)
				tmp[l][r] = a[l][r][i];

		for (int l = 0; l < n; l++)
			for (int r = 0; r < m; r++)
				for (int k = i; k > j + 1; k--)
					a[l][r][k] = a[l][r][k - 1];

		for (int l = 0; l < n; l++)
			for (int r = 0; r < m; r++)
				a[l][r][j + 1] = tmp[l][r];
	}
}


void changeSortP(double *pa, int p) {
	double tmp;
	int k = p - 1, m;
	while (k > 0) {
		m = 0;
		for (int i = 0; i < k; i++)
			if (pa[i] < pa[i + 1] - DBL_EPSILON) {
				tmp = pa[i]; pa[i] = pa[i + 1]; pa[i + 1] = tmp;
				m = i + 1;
			}
		k = m - 1;
	}
}

void changeInd(int *ind, int p) {
	int k = p - 1, m, tmp;
	while (k > 0) {
		m = 0;
		for (int i = 0; i < k; i++)
			if (sum_arr(ind[i]) < sum_arr(ind[i + 1]) - DBL_EPSILON) {
				tmp = ind[i]; ind[i] = ind[i + 1]; ind[i + 1] = tmp;
				m = i + 1;
			}
		k = m - 1;
	}
}

void changeA3d(int p) {
	double tmp;
	int k = p - 1, z;
	while (k > 0) {
		z = 0;
		for (int i = 0; i < k; i++)
			if (sum_arr(i) < sum_arr(i + 1) - DBL_EPSILON) {
				for (int l = 0; l < n; l++)
					for (int r = 0; r < m; r++) {
						tmp = a[l][r][i]; a[l][r][i] = a[l][r][i + 1]; a[l][r][i + 1] = tmp;
					}
				z = i + 1;
			}
		k = z - 1;
	}
}


void shakerSortP(double *pa, int p) {
	int l = 1, r = p - 1;
	double tmp;
	do {
		for (int i = r; i >= l; i--)
			if (pa[i - 1] < pa[i] - DBL_EPSILON) {
				tmp = pa[i];
				pa[i] = pa[i - 1];
				pa[i - 1] = tmp;
			}
		l++;
		for (int i = l; i <= r; i++)
			if (pa[i - 1] < pa[i] - DBL_EPSILON) {
				tmp = pa[i];
				pa[i] = pa[i - 1];
				pa[i - 1] = tmp;
			}
		r--;
	} while (l <= r);
}


void shakerInd(int *ind, int p) {
	int l = 1, r = p - 1, tmp;
	do {
		for (int i = r; i >= l; i--)
			if (sum_arr(ind[i - 1]) < sum_arr(ind[i]) - DBL_EPSILON) {
				tmp = ind[i];
				ind[i] = ind[i - 1];
				ind[i - 1] = tmp;
			}
		l++;
		for (int i = l; i <= r; i++)
			if (sum_arr(ind[i - 1]) < sum_arr(ind[i]) - DBL_EPSILON) {
				tmp = ind[i];
				ind[i] = ind[i - 1];
				ind[i - 1] = tmp;
			}
		r--;
	} while (l <= r);
}


void shakerA3d(int p) {
	int l = 1, r = n - 1;
	double tmp;
	do {
		for (int i = r; i >= l; i--)
			if (sum_arr(i - 1) < sum_arr(i) - DBL_EPSILON) {
				for (int q = 0; q < n; q++)
					for (int d = 0; d < m; d++) {
						tmp = a[q][d][i]; a[q][d][i] = a[q][d][i - 1]; a[q][d][i - 1] = tmp;
					}
			}
		l++;
		for (int i = l; i <= r; i++)
			if (sum_arr(i - 1) < sum_arr(i) - DBL_EPSILON) {
				for (int q = 0; q < n; q++)
					for (int d = 0; d < m; d++) {
						tmp = a[q][d][i]; a[q][d][i] = a[q][d][i - 1]; a[q][d][i - 1] = tmp;
					}
			}
		r--;
	} while (l <= r);
}


int main(void) {
	system("color F0");
	SYSTEMTIME t, t1;

	do {
		cout << "Input size of array A[n,m,p] for n<201, m<201, p<201:\n";
		cout << "n= ";
		cin >> n;
		cout << "m= ";
		cin >> m;
		cout << "p= ";
		cin >> p;
	} while (n > 200 || m > 200 || p > 200);

	int v = 1, out = 1, v2 = 1;
	double an = 0., ak = 0., ahbig = 0., ahsmall = 0.;
	do {
		cout << "\nSelect the way of input array building:\n";
		cout << "1 - sorted array; 2 - opposite sorted array; 3 - random array\n";
		cin >> v;
	} while (v != 1 && v != 2 && v != 3);

	if (v == 3) {
		do {
			cout << "\nSelect the range of array element values: [-2^q; 2^q]\n";
			cout << "q= ";
			cin >> v2;
		} while (v2 < 1 || v2 > 32);
	}
	else
		do {
			cout << "\nSelect the range of array element values: [an; ak]\n";
			cout << "an= ";
			cin >> an;
			cout << "ak= ";
			cin >> ak;
		} while ((ak - an) < DBL_EPSILON);


		ahbig = (ak - an) / (p);
		ahsmall = ahbig / (m*n);

		do {
			cout << "\nWould you like to see sorted array from summes elements by third dimension?\n";
			cout << "1 - yes; 2 - no\n";
			cin >> out;
		} while (out != 1 && out != 2);

		double *sortP = new double[p], tmp;
		int *ind = new int[p];

		inp_arr(v, v2, an, ak, ahbig, ahsmall);
		reset(buff, a);

		//  ���� � �����'����������� �������� ������������
		cout << "\n\n\nExchange with remembering last permutation using 1d array\n";

		for (int k = 0; k < p; k++)
			sortP[k] = sum_arr(k);

		GetLocalTime(&t);

		changeSortP(sortP, p);

		int k = 0, q;
		do {
			for (int j = 0; j < p; j++) {
				if (fabs(sum_arr(k) - sortP[j]) < DBL_EPSILON) {
					q = j;
					break;
				}
			}
			if (q == k)
				k++;
			else
				for (int i = 0; i < n; i++)
					for (int l = 0; l < m; l++) {
						tmp = a[i][l][k];
						a[i][l][k] = a[i][l][q];
						a[i][l][q] = tmp;
					}
		} while (k < p - 1);

		GetLocalTime(&t1);

		printf("\nTime of algorithm working= %d  milliseconds\n", t1.wMilliseconds + t1.wSecond * 1000 + t1.wMinute * 60 * 1000 + t1.wHour * 60 * 60 * 1000 - t.wMilliseconds - t.wSecond * 1000 - t.wMinute * 60 * 1000 - t.wHour * 60 * 60 * 1000);

		if (out == 1) {
			cout << "\nSorted in ascending order array from summes elements by third dimension:\n" << endl;
			for (int i = 0; i < p; i++) {
				cout << setprecision(1) << fixed << sum_arr(i) << "  ";
			}
		}
		cout << "\nPress any key for continue";
		getchar(); getchar();

		reset(a, buff);

		// �������� ����������
		cout << "\n\n\nShaker method using 1d array\n";

		for (int k = 0; k < p; k++)
		sortP[k] = sum_arr(k);

		GetLocalTime(&t);

		shakerSortP(sortP, p);

		k = 0;
		do {
		for (int j = 0; j < p; j++) {
		if (fabs(sum_arr(k) - sortP[j]) < DBL_EPSILON) {
		q = j;
		break;
		}
		}
		if (q == k)
		k++;
		else
		for (int i = 0; i < n; i++)
		for (int l = 0; l < m; l++) {
		tmp = a[i][l][k];
		a[i][l][k] = a[i][l][q];
		a[i][l][q] = tmp;
		}
		} while (k < p - 1);

		GetLocalTime(&t1);

		printf("\nTime of algorithm working= %d  milliseconds\n", t1.wMilliseconds + t1.wSecond * 1000 + t1.wMinute * 60 * 1000 + t1.wHour * 60 * 60 * 1000 - t.wMilliseconds - t.wSecond * 1000 - t.wMinute * 60 * 1000 - t.wHour * 60 * 60 * 1000);

		if (out == 1) {
		cout << "\nSorted in ascending order array from summes elements by third dimension:\n" << endl;
		for (int i = 0; i < p; i++)
			cout << setprecision(1) << fixed << sum_arr(i) << "  ";
		}
		cout << "\nPress any key for continue";
		getchar();

		reset(a, buff);

		//  �������
		cout << "\n\n\nInsert method using 1d array\n";

		for (int k = 0; k < p; k++)
		sortP[k] = sum_arr(k);

		GetLocalTime(&t);

		insertSortP(sortP, p);

		k = 0;
		do {
		for (int j = 0; j < p; j++) {
		if (fabs(sum_arr(k) - sortP[j]) < DBL_EPSILON) {
		q = j;
		break;
		}
		}
		if (q == k) {
		k++;
		}
		else
		for (int i = 0; i < n; i++)
		for (int l = 0; l < m; l++) {
		tmp = a[i][l][k];
		a[i][l][k] = a[i][l][q];
		a[i][l][q] = tmp;
		}
		} while (k < p - 1);

		GetLocalTime(&t1);

		printf("\nTime of algorithm working= %d  milliseconds\n", t1.wMilliseconds + t1.wSecond * 1000 + t1.wMinute * 60 * 1000 + t1.wHour * 60 * 60 * 1000 - t.wMilliseconds - t.wSecond * 1000 - t.wMinute * 60 * 1000 - t.wHour * 60 * 60 * 1000);

		if (out == 1) {
		cout << "\nSorted in ascending order array from summes elements by third dimension:\n" << endl;
		for (int i = 0; i < p; i++)
			cout << setprecision(1) << fixed << sum_arr(i) << "  ";
		}
		cout << "\nPress any key for continue";
		getchar();

		reset(a, buff);

		for (int i = 0; i < p; i++)
		ind[i] = i;

		//  ���� � �����'����������� �������� ������������
		cout << "\n\n\nExchange with remembering last permutation using index vector\n";
		GetLocalTime(&t);

		changeInd(ind, p);

		GetLocalTime(&t1);

		printf("\nTime of algorithm working= %d  milliseconds\n", t1.wMilliseconds + t1.wSecond * 1000 + t1.wMinute * 60 * 1000 + t1.wHour * 60 * 60 * 1000 - t.wMilliseconds - t.wSecond * 1000 - t.wMinute * 60 * 1000 - t.wHour * 60 * 60 * 1000);

		if (out == 1) {
		cout << "\nSorted in ascending order array from summes elements by third dimension:\n" << endl;
		for (int i = 0; i < p; i++)
			cout << setprecision(1) << fixed << sum_arr(ind[i]) << "  ";
		}
		cout << "\nPress any key for continue";
		getchar();

		reset(a, buff);

		for (int i = 0; i < p; i++)
		ind[i] = i;

		//  ����  � �����'����������� �������� ������������
		cout << "\n\n\nShaker method using index vector\n";
		GetLocalTime(&t);

		shakerInd(ind, p);

		GetLocalTime(&t1);

		printf("\nTime of algorithm working= %d  milliseconds\n", t1.wMilliseconds + t1.wSecond * 1000 + t1.wMinute * 60 * 1000 + t1.wHour * 60 * 60 * 1000 - t.wMilliseconds - t.wSecond * 1000 - t.wMinute * 60 * 1000 - t.wHour * 60 * 60 * 1000);

		if (out == 1) {
		cout << "\nSorted in ascending order array from summes elements by third dimension:\n" << endl;
		for (int i = 0; i < p; i++)
			cout << setprecision(1) << fixed << sum_arr(ind[i]) << "  ";
		}
		cout << "\nPress any key for continue";
		getchar(); getchar();

		reset(a, buff);

		for (int i = 0; i < p; i++)
		ind[i] = i;

		//  �������
		cout << "\n\nInsert method using index vector\n";
		GetLocalTime(&t);

		insertInd(ind);

		GetLocalTime(&t1);

		printf("\nTime of algorithm working= %d  milliseconds\n", t1.wMilliseconds + t1.wSecond * 1000 + t1.wMinute * 60 * 1000 + t1.wHour * 60 * 60 * 1000 - t.wMilliseconds - t.wSecond * 1000 - t.wMinute * 60 * 1000 - t.wHour * 60 * 60 * 1000);

		if (out == 1) {
		cout << "\nSorted in ascending order array from summes elements by third dimension:\n" << endl;
		for (int i = 0; i < p; i++)
			cout << setprecision(1) << fixed << sum_arr(ind[i]) << "  ";
		}
		cout << "\nPress any key for continue";
		getchar();

		reset(a, buff);

		//  ����
		cout << "\n\n\nExchange with remembering last permutation of 3d array\n";
		GetLocalTime(&t);

		changeA3d(p);

		GetLocalTime(&t1);

		printf("\nTime of algorithm working= %d  milliseconds\n", t1.wMilliseconds + t1.wSecond * 1000 + t1.wMinute * 60 * 1000 + t1.wHour * 60 * 60 * 1000 - t.wMilliseconds - t.wSecond * 1000 - t.wMinute * 60 * 1000 - t.wHour * 60 * 60 * 1000);

		if (out == 1) {
		cout << "\nSorted in ascending order array from summes elements by third dimension:\n" << endl;
		for (int i = 0; i < p; i++)
			cout << setprecision(1) << fixed << sum_arr(i) << "  ";
		}
		cout << "\nPress any key for continue";
		getchar();

		reset(a, buff);

		//  �������� ����������
		cout << "\n\n\nShaker method of 3d array\n";
		GetLocalTime(&t);

		shakerA3d(p);

		GetLocalTime(&t1);

		printf("\nTime of algorithm working= %d  milliseconds\n", t1.wMilliseconds + t1.wSecond * 1000 + t1.wMinute * 60 * 1000 + t1.wHour * 60 * 60 * 1000 - t.wMilliseconds - t.wSecond * 1000 - t.wMinute * 60 * 1000 - t.wHour * 60 * 60 * 1000);

		if (out == 1) {
		cout << "\nSorted in ascending order array from summes elements by third dimension:\n" << endl;
		for (int i = 0; i < p; i++)
			cout << setprecision(1) << fixed << sum_arr(i) << "  ";
		}
		cout << "\nPress any key for continue";
		getchar(); 

		reset(a, buff);

		//  �������
		cout << "\n\n\nInsert method of 3d array\n";
		GetLocalTime(&t);

		insertA3d(a);

		GetLocalTime(&t1);

		printf("\nTime of algorithm working= %d  milliseconds\n", t1.wMilliseconds + t1.wSecond * 1000 + t1.wMinute * 60 * 1000 + t1.wHour * 60 * 60 * 1000 - t.wMilliseconds - t.wSecond * 1000 - t.wMinute * 60 * 1000 - t.wHour * 60 * 60 * 1000);

		if (out == 1) {
		cout << "\nSorted in ascending order array from summes elements by third dimension:\n" << endl;
		for (int i = 0; i < p; i++)
			cout << setprecision(1) << fixed << sum_arr(i) << "  ";
		}
		getchar(); getchar();

		return 0;
}
//---------------------------------------------------------------------------
