#include <stdlib.h>
#include <iostream>
#include <math.h>
#include <vector>

using namespace std;

int main()
{
    vector <int64_t>A;
    vector <int64_t>B;
    int N, temp;

    cout << "Insert N :" << endl;
    cin >> N;

    if (N < 1 || N > 19) {
        cout << "Int can't hold such things inside." << endl;
        return 0;
    }

    A.reserve(4);

    cout << "--before--\n " << endl;
    for(int i = 0; i < 4; i++) {
        A[i] = 0;

        for(int j = 0; j < N; j++){
            A[i] = A[i]*10 + (rand() % 9) + 1;
        }

        cout << A[i] << endl;
    }



    cout << "\n2. With one array. \n" << endl;

    B.reserve(N);

    for(int i = 0; i < 4; i++) {
        for (int j = N - 1; j >= 0; j--) {
            B[j] = A[i] % 10;
            A[i] = A[i] / 10;
        }

        A[i] = 0;
        if (i != 3) {
            for(int j = N - 1; j >= 0; --j) {
                A[i] = A[i]*10 + B[j];
            }
        } else {
            A[3] = A[0];
            A[0] = 0;
            for (int j = 0; j < N; j++) {
                A[0] = A[0]*10 + B[j];
            }
        }
    }

    for(int i = 0; i < 4; i++) {
        cout << A[i] << endl;
    }

    return 0;
}
