#include <iostream>
#include <conio.h>
#include <dos.h>
#include <graphics.h>

#define pixToRowColFormat(x) {(8*(x - 1))}
#define SPACE 4
#define ROW 10
#define COL 4
#define MAXMENU 4

using namespace std;
typedef char option[17];

option optionMenu[]= {
        "Who did that?",
        "Symbol in 2d...",
        "Symbol in 3d...",
        "Quit"
    };

void showByCoord(int x, int y, char *str) {
   x = pixToRowColFormat(x);
   y = pixToRowColFormat(y);
   outtextxy(x, y, str);
}

void selectedMenu(int x, int y, char *str) {
   x = pixToRowColFormat(x);
   y = pixToRowColFormat(y);
   setcolor(3);

   outtextxy(x, y, str);
   setcolor(WHITE);
}

int menu(int i) {
    settextstyle(TRIPLEX_FONT, HORIZ_DIR, 1);
    setcolor(WHITE);
    int submit = 0, j, key, currentMenu;

    for(j = 0; j <= MAXMENU - 1; j++) {
        if (i == j) {
            selectedMenu(COL, (j * SPACE) + ROW, optionMenu[j]);
        } else {
            showByCoord(COL, (j * SPACE) + ROW, optionMenu[j]);
        }
    }

    do {
       setcolor(WHITE);
       settextstyle(TRIPLEX_FONT, HORIZ_DIR, 1);
       key = getch();

       switch (key) {
         case 00:
            key = getch();

            switch (key) {
               case 72:
                    showByCoord(COL, (i*SPACE)+ROW, optionMenu[i]);
                    i--;
                    if (i == -1) {
                       i = MAXMENU - 1;
                    }

                    selectedMenu(COL,(i*SPACE)+ROW, optionMenu[i]);
                    break;
               case 80:
                    showByCoord(COL, (i*SPACE)+ROW, optionMenu[i]);
                    i++;
           	        if (i == MAXMENU) {
                       i = 0;
           	        }

                    selectedMenu(COL, (i*SPACE)+ROW, optionMenu[i]);
                    break;
            }
            break;
         case 13:
             submit = 1;
      }
   } while (!submit);

   return(i);
}

void showSymbol(int symbolNumber) {
    if (symbolNumber == 1) {
        line(pixToRowColFormat(45), pixToRowColFormat(10), pixToRowColFormat(60), pixToRowColFormat(10));
        line(pixToRowColFormat(60), pixToRowColFormat(10), pixToRowColFormat(70), pixToRowColFormat(45));
        line(pixToRowColFormat(70), pixToRowColFormat(45), pixToRowColFormat(60), pixToRowColFormat(45));
        line(pixToRowColFormat(60), pixToRowColFormat(45), pixToRowColFormat(55), pixToRowColFormat(35));
        line(pixToRowColFormat(55), pixToRowColFormat(35), pixToRowColFormat(50), pixToRowColFormat(35));
        line(pixToRowColFormat(50), pixToRowColFormat(35), pixToRowColFormat(45), pixToRowColFormat(45));
        line(pixToRowColFormat(45), pixToRowColFormat(45), pixToRowColFormat(35), pixToRowColFormat(45));
        line(pixToRowColFormat(35), pixToRowColFormat(45), pixToRowColFormat(45), pixToRowColFormat(10));

        line(pixToRowColFormat(55), pixToRowColFormat(28), pixToRowColFormat(50), pixToRowColFormat(28));
        line(pixToRowColFormat(55), pixToRowColFormat(28), pixToRowColFormat(52.5), pixToRowColFormat(20));
        line(pixToRowColFormat(52.5), pixToRowColFormat(20), pixToRowColFormat(50), pixToRowColFormat(28));
    } else {
        line(pixToRowColFormat(45), pixToRowColFormat(10), pixToRowColFormat(60), pixToRowColFormat(10));
        line(pixToRowColFormat(60), pixToRowColFormat(10), pixToRowColFormat(70), pixToRowColFormat(45));
        line(pixToRowColFormat(70), pixToRowColFormat(45), pixToRowColFormat(60), pixToRowColFormat(45));
        line(pixToRowColFormat(60), pixToRowColFormat(45), pixToRowColFormat(55), pixToRowColFormat(35));
        line(pixToRowColFormat(55), pixToRowColFormat(35), pixToRowColFormat(50), pixToRowColFormat(35));
        line(pixToRowColFormat(50), pixToRowColFormat(35), pixToRowColFormat(45), pixToRowColFormat(45));
        line(pixToRowColFormat(45), pixToRowColFormat(45), pixToRowColFormat(35), pixToRowColFormat(45));
        line(pixToRowColFormat(35), pixToRowColFormat(45), pixToRowColFormat(45), pixToRowColFormat(10));

        line(pixToRowColFormat(55), pixToRowColFormat(28), pixToRowColFormat(50), pixToRowColFormat(28));
        line(pixToRowColFormat(55), pixToRowColFormat(28), pixToRowColFormat(52.5), pixToRowColFormat(20));
        line(pixToRowColFormat(52.5), pixToRowColFormat(20), pixToRowColFormat(50), pixToRowColFormat(28));

        line(pixToRowColFormat(42), pixToRowColFormat(7), pixToRowColFormat(57), pixToRowColFormat(7));
        line(pixToRowColFormat(57), pixToRowColFormat(7), pixToRowColFormat(67), pixToRowColFormat(42));
        line(pixToRowColFormat(67), pixToRowColFormat(42), pixToRowColFormat(57), pixToRowColFormat(42));
        line(pixToRowColFormat(57), pixToRowColFormat(42), pixToRowColFormat(52), pixToRowColFormat(32));
        line(pixToRowColFormat(52), pixToRowColFormat(32), pixToRowColFormat(47), pixToRowColFormat(32));
        line(pixToRowColFormat(47), pixToRowColFormat(32), pixToRowColFormat(42), pixToRowColFormat(42));
        line(pixToRowColFormat(42), pixToRowColFormat(42), pixToRowColFormat(32), pixToRowColFormat(42));
        line(pixToRowColFormat(32), pixToRowColFormat(42), pixToRowColFormat(42), pixToRowColFormat(7));

        line(pixToRowColFormat(52), pixToRowColFormat(25), pixToRowColFormat(47), pixToRowColFormat(25));
        line(pixToRowColFormat(52), pixToRowColFormat(25), pixToRowColFormat(49.5), pixToRowColFormat(17));
        line(pixToRowColFormat(49.5), pixToRowColFormat(17), pixToRowColFormat(47), pixToRowColFormat(25));
// ------------------------------------------------------------------------------------------------------------------//
        line(pixToRowColFormat(45), pixToRowColFormat(10), pixToRowColFormat(42), pixToRowColFormat(7));
        line(pixToRowColFormat(60), pixToRowColFormat(10), pixToRowColFormat(57), pixToRowColFormat(7));
        line(pixToRowColFormat(70), pixToRowColFormat(45), pixToRowColFormat(67), pixToRowColFormat(42));
        line(pixToRowColFormat(60), pixToRowColFormat(45), pixToRowColFormat(57), pixToRowColFormat(42));
        line(pixToRowColFormat(55), pixToRowColFormat(35), pixToRowColFormat(52), pixToRowColFormat(32));
        line(pixToRowColFormat(50), pixToRowColFormat(35), pixToRowColFormat(47), pixToRowColFormat(32));
        line(pixToRowColFormat(45), pixToRowColFormat(45), pixToRowColFormat(42), pixToRowColFormat(42));
        line(pixToRowColFormat(35), pixToRowColFormat(45), pixToRowColFormat(32), pixToRowColFormat(42));

        line(pixToRowColFormat(55), pixToRowColFormat(28), pixToRowColFormat(52), pixToRowColFormat(25));
        line(pixToRowColFormat(52.5), pixToRowColFormat(20), pixToRowColFormat(49.5), pixToRowColFormat(17));
        line(pixToRowColFormat(50), pixToRowColFormat(28), pixToRowColFormat(47), pixToRowColFormat(25));
    }
}

int getMenu() {
   char choice = 0;
   do {
      choice = menu(choice);
      switch (choice) {
         case 0:
            setfillstyle(SOLID_FILL, BLACK);
            bar(pixToRowColFormat(20), pixToRowColFormat(5), pixToRowColFormat(150), pixToRowColFormat(50));
            setcolor(BLUE);
            outtextxy(pixToRowColFormat(35), pixToRowColFormat(10), "st. #6, Zviahintsev Nikolay");
            break;
       	case 1:
       	    setfillstyle(SOLID_FILL, BLACK);
            bar(pixToRowColFormat(20), pixToRowColFormat(5), pixToRowColFormat(150), pixToRowColFormat(50));
            setcolor(BLUE);
            showSymbol(1);
            break;
       	case 2:
            setcolor(BLUE);
            showSymbol(2);
            break;
       	case 3:
            return 0;
      }
   } while (choice != MAXMENU);
}

int main() {

   int gd = DETECT, gm = 0, xScreen, yScreen;
   xScreen = GetSystemMetrics(SM_CXSCREEN);
   yScreen = GetSystemMetrics(SM_CYSCREEN);
   initgraph(&gd, &gm, "");

   settextstyle(BOLD_FONT, HORIZ_DIR, 1);
   setcolor(WHITE);
   outtextxy(pixToRowColFormat(4), pixToRowColFormat(1), "c++, lab_7");

   getMenu();
   closegraph();
}
