#include <iostream>
#include <cmath>
#include <limits>

using namespace std;

int main()
{
    do {
        char exit, type, debug;
        float x, y, z, result;
        float stepOne, stepTwo, stepThree, stepFour, stepFive, stepSix;

        cout << "---- Start variant #6 ----" << endl;
        cout << "Debug mode y/n?" << endl;
        cin >> debug;
        cin.get();

        cout << "Choose formula type a/b?" << endl;
        cin >> type;
        cin.get();


         switch (type)
         {
            case 'a':
                cin.clear();
                cout << "Insert X:" << endl;
                cin >> x;
                cin.get();
                if (cin.fail()) {
                    do {
                        cin.clear();
                        cout << "Insert valid integer X:" << endl;
                        cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                        cin >> x;
                    } while (cin.fail());
                }

                cout << "Insert Y:" << endl;
                cin >> y;
                cin.get();
                if (cin.fail()) {
                    do {
                        cin.clear();
                        cout << "Insert valid integer Y:" << endl;
                        cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                        cin >> y;
                    } while (cin.fail());
                }

                stepOne = pow(y, x+1);
                stepTwo = pow(fabs(y-2)-3, 1.0 / 3.0);
                stepThree = x+(y/2);
                stepFour = 2*fabs(x-y);
                stepFive = stepOne/stepTwo;
                stepSix = stepThree/stepFour;

                result = stepFive + stepSix;

                if (isnan(result)) {
                    cout << "Error. Result is NaN." << endl;
                } else if (isinf(result)) {
                    cout << "Error. Result is infinite." << endl;
                } else {
                    cout << "Result is: " << result << endl;
                }

                if (debug == 'y') {
                    cout << "--Debug trace--"<< endl;
                    cout << "step 1:" << stepOne << endl;
                    cout << "/"<< endl;
                    cout << "step 2:" << stepTwo << endl;
                    cout << "+"<< endl;
                    cout << "step 3:" << stepThree << endl;
                    cout << "/"<< endl;
                    cout << "step 4:" << stepFour << endl;
                    cout << "="<< endl;
                    cout << result << endl;
                }
            break;
        case 'b':
                cin.clear();
                cout << "Insert X:" << endl;
                cin >> x;
                cin.get();
                if (cin.fail()) {
                    do {
                        cin.clear();
                        cout << "Insert valid integer X:" << endl;
                        cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                        cin >> x;
                    } while (cin.fail());
                }

                cout << "Insert Z:" << endl;
                cin >> z;
                cin.get();
                if (cin.fail()) {
                    do {
                        cin.clear();
                        cout << "Insert valid integer Z:" << endl;
                        cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                        cin >> z;
                    } while (cin.fail());
                }

                stepOne = x+1;
                stepTwo = 1/sin(z);
                result = pow(stepOne, stepTwo);

                if (isnan(result)) {
                    cout << "Error. Result is NaN." << endl;
                } else if (isinf(result)) {
                    cout << "Error. Result is infinite." << endl;
                } else {
                    cout << "Result is: " << result << endl;
                }

                if (debug == 'y') {
                    cout << "--Debug trace--"<< endl;
                    cout << "step 1:" << stepOne << endl;
                    cout << "step 2:" << stepTwo << endl;
                    cout << result << endl;
                }
            break;
        }

    cin.clear();
    cout << "Exit y/n?"<< endl;
    cin >> exit;
    cin.get();
    if (exit == 'y') {
        return 0;
    }

    } while (1);
}
