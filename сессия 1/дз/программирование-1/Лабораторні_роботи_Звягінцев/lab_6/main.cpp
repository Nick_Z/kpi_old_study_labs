#include <iostream>
#include <sstream>

using namespace std;

void showString(string targetString) {
    cout << "---------After---------" << endl;
    cout << "Your string : " << endl;
    cout << targetString << endl;
}

string handleString(string targetString) {
    char *rep = "-";
    istringstream sstream(targetString);
    string sub, temp;

    while (sstream >> sub) {
         int position = sub.find_first_of("+");

         if (position != string::npos) {
            if (isdigit(sub[position+1])) {
                sub[position+1] = *rep;
            }
         }

        temp = temp + sub + " ";
    }

    return temp;
}

int main()
{
    string targetString;
    char exit;

    do {
        cin.clear();
        cout << "Insert valid string: \n";

        do {
            cin.clear();
            getline(cin, targetString);
        }
        while (targetString.length() > 255 || targetString.length() < 1);

        targetString = handleString(targetString);

        showString(targetString);

        cout << "continue y/n ?";
        cin >> exit;
        if (exit == 'n' || (exit != 'y' && exit != 'n')) {
            return 0;
        } else {
            targetString.clear();
        }

    } while (1);
}
