#include <iostream>
#include <math.h>
#include <iomanip>
#define PI 3.14159265358979323846
#define E 2.7182818284590452353602874713526624977572470937

using namespace std;

float calculateFunction_4 (float x) {
    return 1 + pow(E, cos(x));
};

float calculateFunction_5 (float x) {
    return 9 + cos(x);
};

float calculateFunction_6 (float x) {
    return ((1 - cos(2*x))/(1 + cos(2*x))) + 4*x;
};

int main()
{
    int n = 7;
    char exit, start;
    float x, result;
    float a = -PI/3.0, b = 2.0*PI, h = (b - a)/n;

    do {
        cin.clear();
        cout << "Start y/n?" << endl;
        cin >> start;
        if (start == 'n') {
            return 0;
        }

        cout << "---------------------------------------------------------------------------------"
             << "----------------------------------------------------------------------\n"
             << "|"
             << setw(20) << "Agrument"
             << setw(20) << "|"
             << setw(20) << "1+e^cos(x)"
             << setw(20) << "|"
             << setw(20) << "9+cos(x)"
             << setw(20) << "|"
             << setw(20) << "tg^2(x)+4x"
             << setw(10) << "|"
             << endl;

        cout << "---------------------------------------------------------------------------------"
             << "----------------------------------------------------------------------\n"
             << endl;

        for (int i = 1; i <= n; i++) {
            x = a + i*h;
            cout << "|"
                 << setw(20) << i
                 << setw(20) << "|"
                 << setw(20) << calculateFunction_4(x)
                 << setw(20) << "|"
                 << setw(20) << calculateFunction_5(x)
                 << setw(20) << "|"
                 << setw(20) << calculateFunction_6(x)
                 << setw(10) << "|"
                 << endl;
        }

        cout << "---------------------------------------------------------------------------------"
             << "----------------------------------------------------------------------\n"
             << endl;

        cout << "Created dy Zviahintsev Mykola. Group #ZKI-zp61, variant #6\n" << endl;

        cout << "Exit y/n?" << endl;
        cin >> exit;
        if (exit == 'y') {
            return 0;
        }

    } while (1);
}
