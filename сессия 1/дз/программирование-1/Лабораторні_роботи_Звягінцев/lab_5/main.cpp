#include <iostream>
#include <fstream>
#include <iomanip>

using namespace std;

struct date {
    int day, month, year;
};

struct grade {
    int programming, sda, mathanalysis, linearAlgebra;
};

struct student
{
    date birthday;
    char surename[20];
    char name[20];
    char secondName[20];
    grade schoolGrade;
};

void show(student *studentArray, int studentNumber) {
    cout << "-------------------------------------------------------------------------------------------"
            << "----------------------------------------------------------------------\n"
            << "|"
            << setw(10) << "Birthday"
            << setw(10) << "|"
            << setw(10) << "Surename"
            << setw(10) << "|"
            << setw(10) << "Name"
            << setw(10) << "|"
            << setw(10) << "Second name"
            << setw(10) << "|"
            << setw(10) << "programming"
            << setw(10) << "|"
            << setw(10) << "sda"
            << setw(10) << "|"
            << setw(10) << "mathanalysis"
            << setw(10) << "|"
            << setw(10) << "linearAlgebra"
            << setw(3) << "|"
            << endl;

    cout << "-------------------------------------------------------------------------------------------"
            << "----------------------------------------------------------------------\n"
            << endl;

    for(int i = 0; i < studentNumber; i++) {
        cout << studentArray[i].birthday.day << ".";
        cout << studentArray[i].birthday.month << ".";
        cout << studentArray[i].birthday.year;
        cout << setw(25);
        cout << studentArray[i].surename;
        cout << setw(22);
        cout  << studentArray[i].name;
        cout << setw(20);
        cout  << studentArray[i].secondName;
        cout << setw(15);
        cout  << studentArray[i].schoolGrade.programming;
        cout << setw(20);
        cout  << studentArray[i].schoolGrade.sda;
        cout << setw(20);
        cout  << studentArray[i].schoolGrade.mathanalysis;
        cout << setw(20);
        cout  << studentArray[i].schoolGrade.linearAlgebra;
        cout << "\n" << endl;
    }
}

student *download (int studentNumber) {
    ifstream instr("input.txt");
    student *studentArray = new student[studentNumber];
        for(int i = 0; i < studentNumber; i++) {
            instr >> studentArray[i].birthday.day;
            instr >> studentArray[i].birthday.month;
            instr >> studentArray[i].birthday.year;
            instr >> studentArray[i].surename;
            instr >> studentArray[i].name;
            instr >> studentArray[i].secondName;
            instr >> studentArray[i].schoolGrade.programming;
            instr >> studentArray[i].schoolGrade.sda;
            instr >> studentArray[i].schoolGrade.mathanalysis;
            instr >> studentArray[i].schoolGrade.linearAlgebra;
        }

    instr.close();

    return studentArray;
};

void sortStudentArray(student *studentArray, string order, int studentNumber) {
    string orderCondition ("asc");
    struct student tempStudent;

    for (int i = 0; i < studentNumber - 1; i++) {
        for (int j = i + 1; j < studentNumber; j++) {
            if (order.compare(orderCondition) != 0) {
                if((studentArray[i].birthday.year * (20*50)
                + studentArray[i].birthday.month * 20
                + studentArray[i].birthday.day)
                >
                (studentArray[j].birthday.year * (20*50)
                + studentArray[j].birthday.month * 20
                + studentArray[j].birthday.day))
                {
                    tempStudent = studentArray[i];
                    studentArray[i] = studentArray[j];
                    studentArray[j] = tempStudent;
                }
            } else {
                if((studentArray[i].birthday.year * (20*50)
                + studentArray[i].birthday.month * 20
                + studentArray[i].birthday.day)
                <
                (studentArray[j].birthday.year * (20*50)
                + studentArray[j].birthday.month * 20
                + studentArray[j].birthday.day))
                {
                    tempStudent = studentArray[i];
                    studentArray[i] = studentArray[j];
                    studentArray[j] = tempStudent;
                }
            }
        }
    }
};

int studentInGroup () {
    string line;
    int studentInGroup = 0;

    ifstream instr("input.txt");
    if(!instr) {
        cout << "File can not be open\n";
        return 0;
    } else {
        while (getline(instr, line)) {
           ++studentInGroup;
        }

        if (studentInGroup == 0 ) {
            cout << "No students";
            return 0;
        }

        instr.clear();
        instr.seekg(0);
        instr.close();
    }

    return studentInGroup;
};

int main()
{
    int studentNumber;
    string order;
    student *studentArray;
    char exit;

    studentNumber = studentInGroup();
    if (studentNumber == 0) {
        return 0;
    }

    studentArray = download(studentNumber);
    cout << "Group" << "\n";
    show(studentArray, studentNumber);

    do {
        cout << "Insert sort condition asc/desc:";
        cin >> order;
        if (order.compare("asc") != 0 && order.compare("desc") != 0) {
            cout << "Default sort: desc" << "\n";
        }

        sortStudentArray(studentArray, order, studentNumber);

        cout << "sorted" << "\n";
        show(studentArray, studentNumber);

        cout << "continue y/n ?";
        cin >> exit;
        if (exit == 'n' || (exit != 'y' && exit != 'n')) {
            return 0;
        }
    } while (1);
}
