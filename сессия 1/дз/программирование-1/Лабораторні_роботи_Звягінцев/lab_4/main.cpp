#include <stdlib.h>
#include <iostream>
#include <math.h>
#include <stdio.h>

using namespace std;

void arrayReverse(int **customArray, int rows, int elments) {
    int temp;
    for (int i = 0; i < rows; i++) {
         for (int j = 0; j < elments/2; j++) {
            temp = customArray[i][j];
            customArray[i][j] = customArray[i][elments - 1 - j];
            customArray[i][elments - 1 - j] = temp;
        }
    }

}

void arrayInit(int **customArray, int N, int M) {
    for(int i = 0; i < N; i++) {
        for(int j = 0; j < M; j++){
            customArray[i][j]= j+1;
        }
    }
}

void arrayShow(int **customArray, int N, int M) {
    cout << "---array A[" << N << "][" << M << "]---" << endl;
    for (int i = 0; i < N; i++) {
         for (int j = 0; j < M; j++) {
            printf("%d\t", customArray[i][j]);
        }
        printf("\n");
    }

    printf("\n");
}

int main()
{
    int **A;
    int **B;
    int N = 4;
    int M = 7;

    A = new int*[N];
    for (int i = 0; i < N; i++) {
        A[i] = new int[M];
    }

    B = new int*[M];
    for (int i = 0; i < M; i++) {
        B[i] = new int[N];
    }

    arrayInit(A, N, M);
    arrayInit(B, M, N);

    arrayShow(A, N, M);
    arrayShow(B, M, N);

    cout << "reverse" << endl;
    printf("\n");
    arrayReverse(A, N, M);
    arrayShow(A, N, M);

    cout << "reverse" << endl;
    printf("\n");
    arrayReverse(B, M, N);
    arrayShow(B, M, N);

    printf("\n");
    cout << "Created by Zviahintsev Mykola. Group #ZKI-zp61, variant #6\n" << endl;

    return 0;
}
