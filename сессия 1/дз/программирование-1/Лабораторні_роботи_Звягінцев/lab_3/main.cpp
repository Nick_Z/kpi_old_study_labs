#include <stdlib.h>
#include <iostream>
#include <math.h>
#include <stdio.h>

using namespace std;

void arrayShow(int **A, int N) {
    cout << "---matrix---" << endl;
    for (int i = 0; i < N; i++) {
         for (int j = 0; j < N; j++) {
            if (!(j%N)) {
                cout<<endl;
            }

            cout << A[i][j] << '\t';
        }
    }

    printf("\n");
}

void arrayInit(int **A, int N) {
    for(int i = 0; i < N; i++) {
        for(int j = 0; j < N; j++){
            A[i][j]= ((2.5*i)+(8.75*j))-j-3;
        }
    }
}

void vectorShow(int **V, int N) {
    cout << "---vector---" << endl;
    for(int i = 0; i < N; i++) {
        printf("%d\t", V[0][i]);
        printf("\n");
    }

    printf("\n");
}

void vectorInit(int **V, int **A, int N) {
    int minValue, maxValue;

    for(int i = 0; i < N; i++) {
        minValue = A[i][0];
        maxValue = A[i][0];
        for(int j = 0; j < N; j++) {
            if (A[i][j] < minValue) {
                minValue = A[i][j];
            }

            if (A[i][j] >= maxValue) {
                maxValue = A[i][j];
            }
        }

        V[0][i] = fabs(maxValue - minValue);
    }
}

void resultShow(int **V, int N) {
    int result;

    cout << "---result(max{xi})---" << endl;

    result = V[0][0];
    for(int i = 0; i < N; i++) {
        if (V[0][i] >= result) {
            result = V[0][i];
        }
    }

    printf("%d\t", result);
    printf("\n");
}

int main()
{
    int **A;
    int **V;
    int N;

    cout << "Insert N:" << endl;
    cin >> N;

    if ((int)N < 1) {
        cout << "Wrong N" << endl;
        return 0;
    }

    A = new int*[N];
    for (int i = 0; i < N; i++) A[i]=new int[N];

    V = new int*[N];
    for (int i = 0; i < N; i++) V[i]=new int[N];

    arrayInit(A, N);
    arrayShow(A, N);
    vectorInit(V, A, N);
    vectorShow(V, N);
    resultShow(V, N);

    printf("\n");
    cout << "Created by Zviahintsev Mykola. Group #ZKI-zp61, variant #6\n" << endl;

    return 0;
}
