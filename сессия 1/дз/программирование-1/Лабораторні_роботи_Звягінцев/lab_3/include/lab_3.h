#include <iostream>
#include <math.h>
#include <stdio.h>
#define N 7

class lab_3
{
    public:
        int createMatrix () {
            int a[N][N];
            for(int i = 0; i < N; i++) {
                for(int j = 0; j < N; j++){
                    a[i][j]= ((2.5*i)+(8.75*j))-j-3;
                }
            }

            return 0;
        }


        int createVector (int a[N][N]) {
            int vect[0][N], minValue, maxValue;
            for(int i = 0; i < N; i++) {
                minValue = a[i][0];
                maxValue = a[i][0];
                for(int j = 0; j < N; j++) {
                    if (a[i][j] < minValue) {
                        minValue = a[i][j];
                    }

                    if (a[i][j] >= maxValue) {
                        maxValue = a[i][j];
                    }
                }

                vect[0][i] = fabs(maxValue - minValue);
            }

            return vect[0][N];
        }

        void showResult (int vect[0][N]) {
            int result = vect[0][0];
            for(int i = 0; i < N; i++) {
                if (vect[0][i] >= result) {
                    result = vect[0][i];
                }
            }

            printf("%d\t", result);
            printf("\n");
        }

    protected:
    private:
        unsigned int _matrix;

};
